#!/usr/bin/env python3

"""
 5.1.Вивести табличку множення, від 1 до 10.
"""
top = "     "
for i in range(1, 11):
    top += f"{i:<3}"

print(top)
print("    " + "-"*(len(top)-5))

for i in range(1, 11):
    print(f"{i:<3}| ", end='', sep = '')
    for j in range(1, 11):
        print(f"{i*j:<3}", end = '', sep = '')
    print()
