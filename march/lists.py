#!/usr/bin/env python3

import random

"""
Task 1

The greatest number

Write a Python program to get the largest number from a list of random numbers with the length of 10

Constraints: use only while loop and random module to generate numbers
"""

arr = []
count = -1

while count < 9:
    arr.append(random.randint(1, 100))
    count += 1

max_value = arr[0]

while count >= 0:
    if arr[count] > max_value:
        max_value = arr[count]
    count -= 1

print(max_value)

"""
Task 2

Exclusive common numbers.

Generate 2 lists with the length of 10 with random integers from 1 to 10, and make a third list containing the common integers between the 2 initial lists without any duplicates.

Constraints: use only while loop and random module to generate numbers
"""
print("\n\n\n")
count = 0

a = []
b = []

while count < 10:
    a.append(random.randint(1, 10))
    count += 1

count = 0
while count < 10:
    b.append(random.randint(1, 10))
    count += 1

result = []
for i in a:
    if i in b and i not in result:
        result.append(i)

print(result)
print("\n\n\n")

"""
Task 3

Extracting numbers.

Make a list that contains all integers from 1 to 100, then find all integers from the list that are divisible by 7 but not a multiple of 5, and store them in a separate list. Finally, print the list.

Constraint: use only while loop for iteration
"""

arr = []
count = 1

while count <= 100:
    arr.append(count)
    count += 1

count = 0
result = []

while count < 100:
    if arr[count] % 7 == 0 and arr[i] % 5 != 0:
        result.append(arr[count])
    count += 1

print(result)




