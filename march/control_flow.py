#!/usr/bin/env python3
from random import randint
"""
Task 1

String manipulation

Write a Python program to get a string made of the first 2 and the last 2 chars from a given string. If the string length is less than 2, return instead of the empty string.

Sample String: 'helloworld'

Expected Result : 'held'

Sample String: 'my'

Expected Result : 'mymy'

Sample String: 'x'

Expected Result: Empty String

Tips:

    Use built-in function len() on an input string
    Use positive indexing to get the first characters of a string and negative indexing to get the last characters
"""
def printer(string):
    if len(string) >= 2:
        print(string[:2] + string[-2:])
    else:
        print('Empy String')

a = "helloworld"
b = "my"
c = "x"

printer(a)
printer(b)
printer(c)

print("\n\n\n\n")

"""
Task 2

The valid phone number program.

Make a program that checks if a string is in the right format for a phone number. The program should check that the string contains only numerical characters and is only 10 characters long. Print a suitable message depending on the outcome of the string evaluation.
"""

def phone_checker(number):
    if len(number) != 10:
        print(f"Not valid length of the phone number: {len(number)}")
    elif number.isnumeric():
        print(f"Here is your number: {number}")
    else:
        print("Your phone number is not numeric")
phone_checker("123")
phone_checker("zxczxczxca")
phone_checker("3336669990")


"""
Task 3

The math quiz program.

Write a program that asks the answer for a mathematical expression, checks whether the user is right or wrong, and then responds with a message accordingly.
"""
print("\n\n\n\n\n\n")

def math_quiz():
    
    operation = randint(0, 3)
    a = randint(1, 20)
    b = randint(1, 20)
    result = 0
    sighn = ''

    if operation == 0:
        result = a + b
        sighn = '+'
    elif operation == 1:
        result = a - b
        sighn = '-'
    elif operation == 2:
        result = a * b
        sighn = '*'
    else:
        result = a // b
        sighn = '//'
    
    string = f"Please enter result of expression: {a} {sighn} {b}: "
    user_input = input(string)

    if user_input.isnumeric():
        user_input = int(input(string))
    else:
        user_input = None

    if user_input == result:
        print("Congrats, you know you are right")
    else:
        print(f"Good luck next time, correct answer is: {result}")

for i in range(1, 11):
    print(f"Quiz number {i}:")
    math_quiz()

"""
Task 4

The name check.

Write a program that has a variable with your name stored (in lowercase) and then asks for your name as input. The program should check if your input is equal to the stored name even if the given name has another case, e.g., if your input is “Anton” and the stored name is “anton”, it should return True.
"""
print("\n\n\n\n")

store = input("Please enter name that will be stored: ")
store = store.lower()

user_input = input("Please enter name for check: ")

if store == user_input.lower():
    print("Congrats, your memory is better than bread")
else:
    print("Check you memory")


    
