#!/usr/bin/env python3

"""Task 2

Write a function that takes in two numbers from the user via input(), call the numbers a and b, and then returns the value of squared a divided by b, construct a try-except block which raises an exception if the two values given by the input function were not numbers, and if value b was zero (cannot divide by zero).   
"""

def function():
    a = input("Enter value for a: ")
    b = input("Enter value for b: ")
    result = 0

    try:
        a = int(a)
        b = int(b)
        
        result = a ** a / b
    except ValueError:
        print(f"Unsupported operation for: {type(a)}a:{a} and {type(b)}b:{b}")
    except ZeroDivisionError:
        print(f"Can't divide by b: {b}")
    else:
        return result

print(function())

