#!/usr/bin/env python3
"""
Task 1

Write a function called oops that explicitly raises an IndexError exception when called. Then write another function that calls oops inside a try/except state­ment to catch the error. What happens if you change oops to raise KeyError instead of IndexError?
"""

def oops():
    #raise IndexError # <-- can be handled by another_function()
    raise KeyError # <-- raises error that not handled by another_function()

def another_function():

    try:
        oops()
    except IndexError:
        print("IndexError")

another_function()
