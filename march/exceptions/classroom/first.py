"""
Задача 1. 
Добийтесь отримання виключень типу TypeError, IndexError
"""
try:
    1 / 'a'
except TypeError:
    print("wrong type")

a = [1, 2]

try:
    a[3]
except IndexError:
    print("worng index")
