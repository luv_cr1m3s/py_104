"""
Задача 2.
Обробіть виключення ZeroDivisionError, видайте повідомлення про некоректну операцію.
"""

"""
Задача 4 Обробіть виключення типу ZeroDivisionError, TypeError, IndexError в одному блоці
"""
"""
Задача 5.
 те саме, що в задачі 4, лише на кожен тип помилки – окремий except.
"""
"""
Задача 6.
реалізуйте обробку для TypeError, ZeroDivisionError  та для всіх інших класів виключень.
"""


try:
    b = {1: 2}
    b[2]
    a = [1]
    a[1]
    a[1]#2 + '2'
    1 / 0
except ZeroDivisionError:
    print("can't divide by 0")
except TypeError:
    print("can't add wrong type")
except IndexError:
    print("out of range")
except Exception as e:
    print(f"General: {type(e)}, {e.args}")
