"""
Задача 8.
Реалізуйте логіку роботи з кількома except для різних класів помилок, та блоком else. Коли виконується else ?
"""

try:
#    b = {1: 2}
#    b[2]
#    a = [1]
#    a[1]
#    1 / 0
    1 + 1 == 2
except ZeroDivisionError:
    print("can't divide by 0")
except TypeError:
    print("can't add wrong type")
except IndexError:
    print("out of range")
except Exception as e:
    print(f"General: {type(e)}, {e.args}")
else:
    print("Everything fine")
finally:
    print("Finally!")
