"""
Задача 10. Реалізуйте варіанти з finally:

    коли код працює нормально
    коли спрацьовує виключення і ви його ловите
    коли спрацьовує виключення і ви його пропускаєте

Примітка: except та finally повинні повертати return з різними значеннями. Проаналізуйте, чи все коректно відпрацювало.
"""

def func():
    try:
        1 + 1 == 2
        1 + '1'
    #except Exception as e:
    #    return(type(e))
    except IndexError:
        return "Index error"
    finally:
        return "Everything fine"

print(func())
