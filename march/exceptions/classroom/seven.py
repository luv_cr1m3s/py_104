"""
Задача 7.
При якій умові виконається другий except ?
 try:
  1 / 0
 except ZeroDivisionError:
  print(1)
 except ZeroDivisionError:
  print(2)
"""

try:
    1 / 0
except ZeroDivisionError:
    print(1)
except ZeroDivisionError:
    print(2)
finally:
    print(3)
