"""
Задача 9. В блоці try викиньте виключення з головним класом Exception, передайте кілька паратетрів. В except обробіть це виключення, виведіть параметри.
"""

try:
    raise Exception("a", "s", "d")
except Exception as e:
    print(type(e), e.args)
    x, y, z = e.args
    print(f"x == {x}")
    print(f"y == {y}")
    print(f"z == {z}")
