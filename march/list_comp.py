#!/usr/bin/env python3


sent = """
Task 1
Make a program that has some sentence (a string) on input and returns a dict containing all unique words as keys and the number of occurrences as values.

Make a program that has some sentence (a string) on input and returns a dict containing all unique words as keys and the number of occurrences as values.
"""

sent_list = sent.split(" ")
sent_list = [i for i in sent_list if i.isprintable()]
sent_uniq = set(sent_list)
sent_dict = { word: sent_list.count(word) for word in sent_uniq}

for k, v in sent_dict.items():
    print(f"{k}: {v}")

"""
Task 2

Compute the total price of the stock where the total price is the sum of the price of an item multiplied by the quantity of this exact item.
"""
print("\n\n")
stock = {
    "banana": 6,
    "apple": 0,
    "orange": 32,
    "pear": 15
}
prices = {
    "banana": 4,
    "apple": 2,
    "orange": 1.5,
    "pear": 3
}

total = sum([stock[i] * prices[i] for i in stock.keys()])

print(f"Total: {total}\n\n\n")

"""

Task 3

List comprehension exercise

Use a list comprehension to make a list containing tuples (i, j) where `i` goes from 1 to 10 and `j` is corresponding to `i` squared.

"""

result = [ (i, i * i) for i in range(1, 11)]

print(result, "\n\n\n\n")


"""
Task 4

    Створити лист із днями тижня.
    В один рядок (ну або як завжди) створити словник виду: {1: “Monday”, 2:...
    Також в один рядок або як вдасться створити зворотний словник {“Monday”: 1,
"""

week = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"]

week_dict = { i : week[i-1] for i in range(1, len(week) + 1)}
dict_week = { v : k for k, v in week_dict.items() }

print(week_dict, "\n")
print(dict_week, "\n")
