#!/usr/bin/python3

import os

"""
Task 1
Run the python interpreter via the terminal. 
Get familiar with running python commands in the terminal, work with output, etc.
"""
phrase = "Hello world"
os.system('python3 -c "print(12345)"')
os.system('python3 -c "print(12345/123)"')
os.system('python3 -c "print()"')

"""
Create a python program named "task2", and use the built-in function 'print' in it several times. 
Try to pass "sep", "end" params and pass several parameters separated by commas. 
Also, provide a comment text above each print statement, mentioned above, 
with the expected output after execution of the particular print statement.
"""
# Hello#$#word@
print("Hello", "world", sep="#$#", end="@\n")

# print not something not fun end
print("print", "something", "fun", sep=" not ", end=" end\n\n\n") 

"""
Task 3
Write a program, which has two print statements to print the following text 
(capital letters "O" and "H", made from "#" symbols):
#########
#       #
#       #
#       #
#########

#       #
#       #
#########
#       #
#       #

Pay attention that usage of spaces is forbidden, as well as creating the whole result 
text string using """ """, use '\n' and '\t' symbols instead.
"""
print("#########", "#\t#", "#\t#", "#\t#", "#########", sep="\n", end="\n\n")

print("#\t#", "#\t#", "#########", "#\t#", "#\t#", sep="\n")

