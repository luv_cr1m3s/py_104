#!/usr/bin/env python3

import os
import sys

"""
Task 1

Files

Write a script that creates a new output file called myfile.txt and writes the string "Hello file world!" in it. Then write another script that opens myfile.txt, and reads and prints its contents. Run your two scripts from the system command line. 

Does the new file show up in the directory where you ran your scripts? 

What if you add a different directory path to the filename passed to open?

Note: file write methods do not add newline characters to your strings; add an explicit ‘\n’ at the end of the string if you want to fully terminate the line in the file.
"""

def write_f(filename):
    
    with open(filename, "w") as f:
        f.write("Hello file world!\n")

def read_f(filename):
    
    with open(filename, "r") as f:
        print(f.read())

def remove_f(filename):
    
    print(f"removing {filename}")
    os.remove(filename)

if __name__ == '__main__':

    filename = ''

    if len(sys.argv) < 2:
        print("using default filename 'myfile.txt'")
        filename = "myfile.txt"
    else:
        filename = sys.argv[1]

    write_f(filename)
    read_f(filename)
    remove_f(filename)
