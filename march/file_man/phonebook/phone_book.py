#!/usr/bin/env python3

"""
Extend Phonebook application

Functionality of Phonebook application:

    Add new entries 
    Search by first name 
    Search by last name 
    Search by full name
    Search by telephone number
    Search by city or state
    Delete a record for a given telephone number
    Update a record for a given telephone number
    An option to exit the program
    ***
    Sort phonebook by phone number

 

The first argument to the application should be the name of the phonebook. 
Application should load JSON data, if it is present in the folder with application,
else raise an error.
After the user exits, all data should be saved to loaded JSON.
"""

import json
import sys
import os
#{ database["phone_number"] :
#                   {"name": '',
#                    "last_name": '',
#                    "full_name": '',
#                    "city" :'',
#                    "state" : ''}

# colors for terminal
RESET = '\033[0m'
RED = '\033[31m'
GREEN = '\033[32m'
BLUE = '\033[34m'
PURPLE = '\033[35m'
CYAN = '\033[36m'
YELLOW = '\033[93m'
PINK = '\033[95m'

def write_phone(database):

    phone_number = input(f"{PURPLE}Please enter phone number: {RESET}")
    data = {"name": "", "last_name": "", "full_name": "", "city": "", "state": ""}

    for key in data.keys():
        data[key] = input(f"{BLUE}Enter value for {key}: {RESET}")
    database[phone_number] = data

def delete_record(database):

    phone = input(f"{PINK}Enter phone number to be removed: {RESET}")

    if phone in database.keys():
        print(f"Going to delete: {phone}: {database[phone]}")
        del database[phone]
    else:
        print(f"{RED}No such phone in database: {phone}{RESET}")
        print_data(database)

def update_record(database):

    phone = input(f"{BLUE}Enter phone number to be updated: {RESET}")

    if phone not in database.keys():
        print(f"No record for: {phone}")
        return

    for key in database[phone].keys():
        user_data = input(f"{GREEN}Enter new {key} or 's' for skip: {RESET}")
        if user_data != 's':
            database[phone][key] = user_data
        else:
            print(f"{CYAN}Value stil the same: {key} -- {database[phone][key]}{RESET}")



def find_phone(database):

    phone = input("Enter phone number that you are looking for: ")

    if phone in database.keys():
        print_data({phone: database[phone]})
    else:
        print(f"{RED}Can't find such phone number: {phone}{RESET}")



def load_data(filename):

    print(f"{GREEN}Loading data from {filename}{RESET}")
    data = {}
    try:
        with open(filename) as f:
            if not os.stat(filename).st_size == 0:
                data = json.load(f)
    except FileNotFoundError:
        print(f"Can't find {filename}")
        quit(1)

    return data

def write_data(data, filename):
    
    database_file = open(filename, "w")
    json.dump(data, database_file)
    database_file.close()

def print_data(database):

    for key in database.keys():
        print(f"{GREEN}{key}:{RESET}")
        for k, v in database[key].items():
            print(f"\t{YELLOW}{k}: {v}{RESET}")

def help():
    print("""
            1. Add new entries 
            2. Search by first name 
            3. Search by last name 
            4. Search by full name
            5. Search by telephone number
            6. Search by city or state
            7. Delete a record for a given telephone number
            8. Update a record for a given telephone number
            p/print Print database
            s/sort Sort phonebook
            h/help Print help
            e/exit Exit the program 
            """)

def sort_db(database):
    
    print("Sorting phonebook by phone number")
    result = {key:database[key] for key in sorted(database.keys())}
    
    return result


def search_by_value(database, field):

    result = {}

    value = input(f"{PURPLE}Enter {field} value for search:{RESET}")

    for key in database.keys():
        if database[key][field] == value:
            result[key] = database[key]

    if len(result.keys()) > 0:
        print(f"{GREEN}Found {value} for {field} in: {RESET}")
        print_data(result)
    else:
        print(f"{RED}No records for {field} --> {value}{RESET}")


def phonebook(filename):

    if not os.path.isfile(filename):
        raise FileNotFoundError

    print("Wellcome to simple phonebook app")
    help()
    database = load_data(filename)
    print_data(database)

    user_input = ''

    while "e" not in (user_input := input("Enter action: ")):
        if 'h' in user_input:
            help()
        elif 'p' in user_input:
            print_data(database)
        elif 's' in user_input:
            database = sort_db(database)
        elif user_input == '1':
            write_phone(database)
        elif user_input == '2':
            search_by_value(database, "name")
        elif user_input == '3':
            search_by_value(database, "last_name")
        elif user_input == '4':
            search_by_value(database, "full_name")
        elif user_input == '5':
            find_phone(database)
        elif user_input == '6':
            print("Enter 'c' if you want to search by city")
            print("Enter 's' if you want to search by state")
            print("Enter 'b' to search by both")

            direction = input()
            if direction == 'b':
                search_by_value(database, "city")
                search_by_value(database, "state")
            elif direction == 'c':
                search_by_value(database, "city")
            elif direction == 's':
                search_by_value(database, "state")
            else:
                print(f"unknown option {direction}")
        elif user_input == '7':
            delete_record(database)
        elif user_input == '8':
            update_record(database)
        else:
            print(f"Unknown option: {user_input}")
        print(f"\n{'-*-'*30}\n")

    write_data(database, filename)

if __name__ == '__main__':
    if len(sys.argv) == 2:
        filename = sys.argv[1]
    else:
        print("Pls provide filename: ./phone_book.py data.json")
        exit(1)
    phonebook(filename)
