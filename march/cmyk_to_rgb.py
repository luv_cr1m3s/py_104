#!/usr/bin/env python3

"""
Type conversion. Several different formats are used to represent color. For example, the primary format for LCD displays, digital cameras, and web pages—known as the RGB format—specifies the level of red (R), green (G), and blue (B) on an integer scale from 0 to 255. The primary format for publishing books and magazines—known as the CMYK format—specifies the level of cyan (C), magenta (M), yellow (Y), and black (K) on a real scale from 0.0 to 1.0.

Create a program that after the start, indefinitely waits for user input in CMYK format and on each input converts to RGB and prints response in the format (equal sign should be on the same level):
 red = 255
 green = 0
 blue = 255

Formula:
white = 1 − black
red = 255 × white × (1 − cyan)
green = 255 × white × (1 − magenta)
blue = 255 × white × (1 − yellow)
 
As the input program waits for parameters like 0.0 1.0 0.0 0.0 (4 numbers split by space character).
The program should terminate after receiving as input exit or e string. Also, the program needs to have logic to check for the right input format and in case of passing wrong parameters should print a suitable message via print and continue waiting for input.
"""

def cmyk_converter(cmyk):
    rgb = {"red": 0, "green": 0, "blue": 0}
    
    white = 1 - cmyk[3]
    
    rgb["red"] = int(255 * white * (1 - cmyk[0]))
    rgb["green"] = int(255 * white * (1 - cmyk[1]))
    rgb["blue"] = int(255 * white * (1 - cmyk[2]))

    return rgb

if __name__ == '__main__':
    
    print("Welcome to CMYK to RGB convertor, to exit type \'e\' or \'exit\'")
    print("CMYK color expected to be entered in format: \'1.0 1.0 1.0 1.0\'")
    
    user_input = input("Enter CMYK color value separated by space: ")

    while user_input != "exit" and user_input != "e":
        color_keys = []
        rgb = {}
        failed_input = False

        if (keys := len(user_input.split(" "))) != 4:
            print(f"Wrong format, expected 4 color keys, got {keys}")
            continue
        else:
            color_keys = user_input.split(" ")
        
        for i in color_keys:
            tmp = i.replace('.', '', 1)
            if not tmp.isdigit():
                print(f"Wrong format for color key: {i}")
                failed_input = True
            else:
                if not 0 <= float(i) <= 1 :
                    print(f"Wrong color key value: {i}")
                    failed_input = True
        
        if not failed_input:
            color_keys = [float(i) for i in color_keys]
            print(color_keys) 
            rgb = cmyk_converter(color_keys)
            print(f"Result for cmyk {color_keys}:") 
            for key, value in rgb.items():
                print(f"{key:5} = {value:3}")

        user_input = input("Enter CMYK color value separated by space: ")
    
    print("Exiting program")
