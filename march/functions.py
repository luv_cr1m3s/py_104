#!/usr/bin/env python3

"""
Task 1

A simple function.

Create a simple function called favorite_movie, which takes a string containing the name of your favorite movie. The function should then print “My favorite movie is named {name}”.
"""

def favorite_movie(movie):
    print(f"My favorite movie is named {movie}")

favorite_movie("Mummy")
favorite_movie("Trainspoting")
favorite_movie("Shrek")
print("\n\n\n\n")

"""
Task 2

Creating a dictionary.

Create a function called make_country, which takes in a country’s name and capital as parameters. Then create a dictionary from those two, with ‘name’ as a key and ‘capital’ as a parameter. Make the function print out the values of the dictionary to make sure that it works as intended.
"""

def make_country(country, capital):

    result = {country: capital}

    print(result)

make_country("Ukraine", "Kyiv")
make_country("Germany", "Berlin")

print("\n\n\n\n")

"""
Task 3

A simple calculator.

Create a function called make_operation, which takes in a simple arithmetic operator as a first parameter (to keep things simple let it only be ‘+’, ‘-’ or ‘*’) and an arbitrary number of arguments (only numbers) as the second parameter. Then return the sum or product of all the numbers in the arbitrary parameter. For example:

    the call make_operation(‘+’, 7, 7, 2) should return 16
    the call make_operation(‘-’, 5, 5, -10, -20) should return 30
    the call make_operation(‘*’, 7, 6) should return 42  
"""

def sub(args):
    result = args[0] * 2

    for i in args:
        result -= i
    return result

def mult(args):
    result = 1

    for i in args:
        result *= i

    return result

def div(args):
    result = args[0]**2

    for i in args:
        result /= i

    return result

def make_operation(ar_op, *args):

    for i in args:
        if type(i) != int and type(i) != float:
            print(f"Can't provide arithmetic operations for non-numeric types {i}: {type(i)}")
            return None
    
    if type(ar_op) != str or ar_op not in '+-/*':
        print(f"Can't provide such operation: {ar_op}")
        return None

    functions = {'+': sum, '-': sub, '*': mult, '/': div}
    result = functions[ar_op](args)

    return result

print(make_operation('+', 7, 7, 2))
print(make_operation('-', 5, 5, -10, -20))
print(make_operation('*', 7, 6))
print(make_operation('/', 49, 1, 7, 7, 7))
print(make_operation('/', 49.0, 1, 7, 7, 7))
print(make_operation('|', 49, 1, 7, 7, 7))
print(make_operation('/', '49', 1, 7, 7, 7))
print(make_operation(123, 49, 1, 7, 7, 7))


