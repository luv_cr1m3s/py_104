#!/usr/bin/env python3
from random import randint
from random import shuffle

"""Task 1

The Guessing Game.

Write a program that generates a random number between 1 and 10 and lets the user guess what number was generated. The result should be sent back to the user via a print statement.
"""

number1 = randint(1, 10)
user_input = int(input("Please enter number for start: "))


while user_input != number1:
    if user_input >  number1:
        print("Input number is too high")
    else:
        print("Input number is too low")
    user_input = int(input("Enter your gues: "))

print("Congrats! You are right")

print("\n\n--------------------------------------------\n\n")

"""
Task 2

The birthday greeting program.

Write a program that takes your name as input, and then your age as input and greets you with the following:

“Hello <name>, on your next birthday you’ll be <age+1> years”
"""

name = input("Enter your name: ")
age = input("Enter your age: ")

print(f"Hello {name}, on your next birthday you’ll be {int(age)+1} years")

print("\n\n--------------------------------------------\n\n")


"""
Task 3

Words combination

Create a program that reads an input string and then creates and prints 5 random strings from characters of the input string.

For example, the program obtained the word ‘hello’, so it should print 5 random strings(words) that combine characters
"""
user_input = input("Enter your word: ")
arr = []

for letter in user_input:
    arr.append(letter)

for i in range(0, 5):
    shuffle(arr)
    print(''.join(arr))


