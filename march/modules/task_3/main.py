#!/usr/bin/env python3
import sys
from mymod import test

if __name__ == '__main__':
    
    if len(sys.argv) != 2:
        print("Usage: ./main.py filename")

    test(sys.argv[1])
