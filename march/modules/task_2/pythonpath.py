"""
Task 2

The sys module.

The “sys.path” list is initialized from the PYTHONPATH environment variable. Is it possible to change it from within Python? If so, does it affect where Python looks for module files? Run some interactive tests to find it out.
 """

import sys
import os

env_path = []
env_path = os.popen('echo $PATH').read().split(":")
env_path = [i.rstrip() for i in env_path]

print(f"sys.path: {sys.path}\n")
print(f"$PATH env variable: {env_path}\n")
print(f"$PYTHONPATH env variable: {os.popen('echo $PYTHONPATH').read()}")

print(f"\nCommon dirs in sys.path and $PATH: {set(sys.path) & set(env_path)}")

#Danger zone
print("Here we test customization of sys.path")
print("""Script will create python module named 'useless.py',
add '/home/uname/' to sys.path and import useless.py. 
After that uselss.py will be removed""")

user_input = input("Do you want to procedd? (y/n): ")

if user_input == 'y':
    uname = os.popen("whoami").read().rstrip()
    os.system(f"echo \"print('Hello from the other side')\" > /home/{uname}/useless.py")
    sys.path.append(f"/home/{uname}")

    import useless
    print(sys.path)
    foo = input("Press to finish")
    os.system(f"rm /home/{uname}/useless.py")
