#!/usr/bin/env python3

"""
Task 1

The greeting program.

Make a program that has your name and the current day of the week stored as separate variables and then prints a message like this:

     "Good day <name>! <day> is a perfect day to learn some python."

Note that  <name> and <day> are predefined variables in source code.

An additional bonus will be to use different string formatting methods for constructing result string.
"""

day = "Monday"
name = "Oleksii"

print(f"Good day {name}! {day} is a perfect day to learn some python.")
print("Good day {}! {} is a perfect day to learn some python.".format(name, day))
print("Good day %s! %s is a perfect day to learn some python." % (name, day))


"""
Task 2

Manipulate strings.

Save your first and last name as separate variables, then use string concatenation to add them together with a white space in between and print a greeting.
"""

name = "Oleksii"
last_name = "Parilov"
print("Greetings, " + name + " " + last_name + "!")

"""
Task 3

Using python as a calculator.

Make a program with 2 numbers saved in separate variables a and b, then print the result for each of the following: 

    Addition
    Subtraction
    Division
    Multiplication
    Exponent (Power)
    Modulus
    Floor division
"""

a = 123456789
b = 987654321

print(f"{a} + {b} = {a + b}")
print(f"{a} - {b} = {a - b}")
print(f"{a} / {b} = {a / b}")
print(f"{a} * {b} = {a * b}")
print(f"{a} ** {b//100000000} = {a ** (b//100000000)}")
print(f"{a} % {b} = {a % b}")
print(f"{a} // {b} = {a // b}")
