#!/usr/bin/env python3

"""
2D random walk.
A two-dimensional random walk simulates the behavior of a particle moving in a grid of points.At each step, the random walker moves north, south, east, or west with probability equal to 1/4, independent of previous moves. Write a program that takes an integer command-line argument n and estimates how long it will take a random walker to hit the boundary of a 2n-by-2n square centered at the starting poin
"""
from os import system
from time import sleep
from random import randint


def print_field(field):
    """
    takes 2d array with values 0, 1, 2
    and prints it to terminal
    """

    length = len(field[0])
    result = []
    start = f"┌{'──'*length}┐"
    end = f"└{'──'*length}┘"

    result.append(start)

    mapper = ["⬜", "⬛", "\033[92m⬛\033[0m"]

    for i in field:
        line = ''
        for cel in i:
            line += mapper[cel]
        result.append(f"│{line}│")

    result.append(end)

    print("\n".join(result))


def find_walker(field):
    """
    finds coordinates of waler
    """

    result = None
    for i in field:
        if 2 in i:
            result = [field.index(i), i.index(2)]
    return result


def walk(result):
    """
    randomly moves walker by one step
    """

    y, x = find_walker(result)

    # 0 -- north, 1 -- south, 2 -- east, 3 -- west
    direction = randint(0, 4)
    print(f"Direction: {direction}")
    result[y][x] = 1

    if direction == 0:
        y += 1
    elif direction == 1:
        y -= 1
    elif direction == 2:
        x += 1
    else:
        x -= 1

    result[y][x] = 2


def finish_detect(field):
    """
    detects if walker reached border of field
    """

    x, y = find_walker(field)
    size = len(field) - 1

    if x == 0 or y == 0:
        return True
    if x == size or y == size:
        return True

    return False


if __name__ == "__main__":

    field_size = int(input("Input size of the fiedl, it will be doubled: "))
    field_len = field_size * 2
    arr = [[0 for x in range(field_len)] for y in range(field_len)]

    # initialize walker position
    # in the mid of the field
    arr[field_size-1][field_size-1] = 2
    count = 1

    while not finish_detect(arr):
        system("clear")
        walk(arr)

        print(f"Field after {count} iteration")
        print(find_walker(arr))

        print_field(arr)
        count += 1
        sleep(0.2)
