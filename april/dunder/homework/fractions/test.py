#!/usr/bin/env python3

import unittest
from fraction import Fraction

class TestFraction(unittest.TestCase):

    def test_creation(self):
        self.assertTrue(Fraction(2, 3))
    
    def test_creation_typecheck(self):
        with self.assertRaises(TypeError):
            Fraction(1, 4.0)
        with self.assertRaises(TypeError):
            Fraction(False, 4)
        with self.assertRaises(TypeError):
            Fraction(1, "4")
        with self.assertRaises(TypeError):
            Fraction((1, ), 4)
        with self.assertRaises(TypeError):
            Fraction(1, {4:4})

    def test_creation_zerocheck(self):
        with self.assertRaises(ZeroDivisionError):
            Fraction(1, 0)

    def test_addition_error(self):
        a1 = Fraction(1, 1)
        with self.assertRaises(TypeError):
            a1 + 4
        with self.assertRaises(TypeError):
            a1 + 1/1
        with self.assertRaises(TypeError):
            a1 + "asd"
        with self.assertRaises(TypeError):
            a1 + (1, )
        with self.assertRaises(TypeError):
            a1 + {1:1}


    def test_addition_simple(self):
        a1 = Fraction(1, 2)
        a2 = Fraction(2, 1)
        self.assertEqual(f"{a1+a2}", "5/2" )

        a3 = Fraction(0, 2)
        self.assertEqual(f"{a1+a3}", "1/2" )
        
        a4 = Fraction(-1, 2)
        self.assertEqual(f"{a1+a4}", "0/0")


    def test_substitution_simple(self):
        a1 = Fraction(1, 2)
        a2 = Fraction(2, 1)
        self.assertEqual(f"{a1-a2}", "-3/2" )

        a3 = Fraction(0, 2)
        self.assertEqual(f"{a1-a3}", "1/2" )
        
        a4 = Fraction(-1, 2)
        self.assertEqual(f"{a1-a4}", "1/1")

    def test_substitution_simple(self):
        a1 = Fraction(1, 2)
        a2 = Fraction(2, 1)
        self.assertEqual(f"{a1*a2}", "1/1" )

        a3 = Fraction(0, 2)
        self.assertEqual(f"{a1*a3}", "0/0" )
        
        a4 = Fraction(-1, 2)
        self.assertEqual(f"{a1*a4}", "-1/4")  


    def test_division_simple(self):
        a1 = Fraction(1, 2)
        a2 = Fraction(2, 1)
        self.assertEqual(f"{a1/a2}", "1/4" )

        a3 = Fraction(0, 2)
        self.assertEqual(f"{a1/a3}", "0/0" )
        
        a4 = Fraction(-1, 2)
        self.assertEqual(f"{a1/a4}", "-1/1")  

    def test_equality_simple(self):
        a1 = Fraction(1, 2)
        a2 = Fraction(2, 1)
        a3 = Fraction(1, 2)

        self.assertEqual(a1 == a2, False )
        self.assertEqual(a1 != a2, True)

        with self.assertRaises(TypeError):
            a1 == "asd"
        
        self.assertEqual(a1 == a3, True )
        self.assertEqual(a1 != a3, False)

    def test_comp_simple(self):
        a1 = Fraction(1, 2)
        a2 = Fraction(2, 1)
        a3 = Fraction(1, 2)

        self.assertEqual(a1 > a2, False)
        self.assertEqual(a1 < a2, True)
        self.assertEqual(a1 < a3, False)

        with self.assertRaises(TypeError):
            a1 < 1.0

    def test_compeq_simple(self):
        a1 = Fraction(1, 2)
        a2 = Fraction(2, 1)
        a3 = Fraction(1, 2)

        self.assertEqual(a1 >= a2, False)
        self.assertEqual(a1 <= a2, True)
        self.assertEqual(a1 <= a3, True)

        with self.assertRaises(TypeError):
            a1 < 1.0

if __name__ == '__main__':
    unittest.main()
