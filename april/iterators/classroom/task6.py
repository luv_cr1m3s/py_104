"""
6. Product
Сформуйте варіанти з :
'AB', "ab", 'ef'

"""
import itertools
a = ['AB', "ab", 'ef']

c = itertools.product(a, repeat = 2)
print(*c)


"""
7. combinations
Сформуйте комбінації по 3 символи з набору „ABCDE“
"""

xc = "ABCDE"

d = itertools.combinations(xc, r=3)

print("\n\n\n\n", *d)

"""
8. permutations
Сформуйте комбінації по 3 символи з набору „ABCDE“
"""

p = itertools.permutations(xc, r=3)
print("\n\n\n", *p)
