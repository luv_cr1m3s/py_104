"""
5. StarMap
Даний список a_list = [[1,2,3], [4,5,6,7,8,89,7,6,4,3,1,3,2], [0]]
Поверніть список максимальних значень вкладених списків.
"""
import itertools
a_list = [[1,2,3], [4,5,6,7,8,89,7,6,4,3,1,3,2], [0]]

def foo(*ll):
    return max(ll)

c = list(itertools.starmap(foo, a_list))
print(*c)
