"""
Задача 1.
Реалізувати:

    ітератор (через клас),
    функцію-генератор,
    вираз-генератор

для роботи з числами в діапазоні від 0 до 100 000 000 000, які без остачі діляться на 3 та на 7.
"""

class MyIter:

    def __init__(self, start, end):
        self.current = start
        self.start = start
        self.end = end

    def __iter__(self):
        self.current = self.start 
        return self

    def __next__(self):
        while self.current < self.end:
            if self.current % 3 == 0 and self.current % 7 == 0:
                result = self.current
                self.current += 1
                return result
            self.current += 1
        raise StopIteration
        

def f_gen(start, end):
    current = start - 1
    
    while current < end:
        current += 1 
        if current % 3 == 0 and current % 7 == 0:
            yield current


f_g = f_gen(0, 100)

strange_gen = (i for i in range(0, 100) if i % 3 == 0 and i % 7 == 0)


myt = MyIter(0, 100)

for i in range(0, 5):
    print(next(myt))

