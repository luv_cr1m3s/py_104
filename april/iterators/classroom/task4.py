"""
4. Takewhile
Даний список a_list = [(1, "B"), (2, "C"), (3, "A"), (4, "D"), (4, "D")]
Сформуйте результуючий список , що відповідає умові x[0] < 3
"""

import itertools
a_list = [(1, "B"), (2, "C"), (3, "A"), (4, "D"), (4, "D")]
c = itertools.takewhile(lambda x: x[0] < 3, a_list)

print(*c)


