"""
Task 2

Create your own implementation of a built-in function range, named in_range(), which takes three parameters: `start`, `end`, and optional step. Tips: See the documentation for `range` function

"""

def in_range(start, end, step = 1):
    tmp = start
    while tmp < end:
        yield tmp
        tmp += step

a = [i for i in in_range(0, 30, 3)]
print(*a)
b = [i for i in in_range(10, 40, 7)]
print(*b)
