"""
Task 1

Create your own implementation of a built-in function enumerate, named `with_index`, which takes two parameters: `iterable` and `start`, default is 0. Tips: see the documentation for the enumerate function
"""

def with_index(iterable, start=0):
    index = start
    for i in iterable:
        yield (index, i)
        index+=1

a = "abcdefghij"
b = with_index(a, 100)

for i in b:
    print(i)

c = [i for i in range(10)]
d = with_index(c, -1)

for i in d:
    print(i)
