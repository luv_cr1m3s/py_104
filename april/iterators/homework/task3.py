"""
Task 3

Create your own implementation of an iterable, which could be used inside for-in loop. Also, add logic for retrieving elements using square brackets syntax.
"""

class Iterable:

    def __init__(self, start, end):
        self.start = start
        self.end = end

    
    def __iter__(self):
        return Iterator(self.start, self.end) 

    def __getitem__(self, index):
        if index > self.end or index < self.start :
            raise IndexError
        return f"index[{index}]:{self.start+index}"

    def __len__(self):
        return self.end-self.start


class Iterator:


    def __init__(self, start, end):
        self.start = start
        self.end = end
        self.__iter__()

    def __iter__(self):
        self.current = self.start
        return self

    def __next__(self):
        if self.current > self.end:
            raise StopIteration
        value = self.current
        self.current += 1 
        return value


a = Iterable(0, 10) 

print("TEST next():")

print("\nTEST for:")

for i in a:
    print(i)

# raises error because Iterable is not an iterator object
# print(next(a))

print("\nTEST []:")

for i in range(0, len(a)+1):
    print(a[i])
