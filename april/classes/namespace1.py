#!/usr/bin/env python3

from my_module import print_p

print_p(print_p)

def print_p(parameter):
    print(f"Hello! I'm {str(parameter.__class__).split(' ')[1][1:-2]} {parameter.__name__} from module: {parameter.__module__}")

print_p(print_p)
