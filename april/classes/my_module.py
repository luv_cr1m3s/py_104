# file: my_module.py 
def print_p(parameter):
    print(f"Hello! I'm {str(parameter.__class__).split(' ')[1][1:-2]} {parameter.__name__} from module: {parameter.__module__}")
