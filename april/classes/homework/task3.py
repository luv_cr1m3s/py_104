#!/usr/bin/env python3

"""
Task 3

TV controller

Create a simple prototype of a TV controller in Python. It’ll use the following commands:

    first_channel() - turns on the first channel from the list.
    last_channel() - turns on the last channel from the list.
    turn_channel(N) - turns on the N channel. Pay attention that the channel numbers start from 1, not from 0.
    next_channel() - turns on the next channel. If the current channel is the last one, turns on the first channel.
    previous_channel() - turns on the previous channel. If the current channel is the first one, turns on the last channel.
    current_channel() - returns the name of the current channel.
    is_exist(N/'name') - gets 1 argument - the number N or the string 'name' and returns "Yes", if the channel N or 'name' exists in the list, or "No" - in the other case.

 

The default channel turned on before all commands is №1.

Your task is to create the TVController class and methods described above.

"""

class TVController:
    
    def __init__(self, channels):

        self.channels = channels
        self.c_channel = 0
    
    def current_channel(self):
        return self.channels[self.c_channel]

    def turn_channel(self, channel_n):
        if channel_n >= 1 and channel_n <= len(self.channels):
            self.c_channel= channel_n - 1
        return self.current_channel()
    
    def first_channel(self):
        self.c_channel= 0
        return self.current_channel()

    def last_channel(self):
        self.c_channel= len(self.channels) - 1
        return self.current_channel()
   
    def previous_channel(self):
        self.c_channel-= 1
        if self.c_channel< 0:
            self.c_channel= len(self.channels) - 1
                                                     
        return self.current_channel()

    def next_channel(self):
        self.c_channel+= 1
        if self.c_channel> len(self.channels):
            self.c_channel= 0

        return self.current_channel()

    def is_exist(self, channel_name):
        if channel_name in self.channels:
            return "Yes"
        else:
            return "No"

CHANNELS = ["BBC", "Discovery", "TV1000"]

controller = TVController(CHANNELS)
assert(controller.first_channel() == "BBC")
assert(controller.last_channel() == "TV1000")
assert(controller.turn_channel(1) == "BBC")
assert(controller.next_channel() == "Discovery")
assert(controller.previous_channel() == "BBC")
assert(controller.current_channel() == "BBC")
assert(controller.is_exist(4) == "No")
assert(controller.is_exist("BBC") == "Yes")

print("ALL TESTS PASSED")
