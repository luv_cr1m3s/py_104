#!/usr/bin/env python3

"""
Task 2

Doggy age

Create a class Dog with class attribute `age_factor` equals to 7. Make __init__() which takes values for a dog’s age. Then create a method `human_age` which returns the dog’s age in human equivalent.
"""

class Dog:
    age_factor = 7

    def __init__(self, age):
        self.age = age

    def human_age(self):
        return self.age * Dog.age_factor

dog1 = Dog(13)
print(f"Age in human time: {dog1.age}")
print(f"Age in dogs time: {dog1.human_age()}")

