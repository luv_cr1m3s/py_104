#!/usr/bin/env python3

def foo():
    a = 123
    b = 456
    c = 788

    print(a * b * c)


"""
Task 1

Write a Python program to detect the number of local variables declared in a function.
"""

def arg_len(func):
    return func.__code__.co_nlocals

print(arg_len(foo))
print("\n\n")
"""
Task 2

Write a Python program to access a function inside a function (Tips: use function, which returns another function)
"""

def ret_func(func):
    return func()

ret_func(foo)
print("\n\n")
"""
Task 3

Write a function called `choose_func` which takes a list of nums and 2 callback functions. If all nums inside the list are positive, execute the first function on that list and return the result of it. Otherwise, return the result of the second one

"""

def choose_func(array, func_1, func_2):
    sign = list(filter(lambda x: x >= 0, array))
    if len(sign) == len(array):
        return func_1(array)
    return func_2(array)

# Assertions

nums1 = [1, 2, 3, 4, 5]

nums2 = [1, -2, 3, -4, 5]


def square_nums(nums):

    return [num ** 2 for num in nums]


def remove_negatives(nums):

    return [num for num in nums if num > 0]

print(choose_func(nums1, square_nums, remove_negatives))
print(choose_func(nums2, square_nums, remove_negatives))

assert choose_func(nums1, square_nums, remove_negatives) == [1, 4, 9, 16, 25]

assert choose_func(nums2, square_nums, remove_negatives) == [1, 3, 5]
