"""
Задача 4.
Створити клас клієнта,

    атрибути:

    ПІБ
    дата народження (string)

    методи:

    __init__ (приймає ПІБ, дату народження),
    вивести інформацію про клієнта (ПІБ, дату народження)

Створити клас Рахунок:

    атрибути:
    клієнт (екземпляр класу Клієнт)	
    залишок на рахунку (захищений атрибут !)

	методи:

    __init__ (приймає екземпляр класу Клієнт, залишок на рахунку встановлює 0)

    змінити залишок на рахунку (приймає від“ємне або додатнє число) (захищений метод !)
    вивести залишок на рахунку

Створити клас gold-Рахунок (наслідує Рахунок):

    метод залишку на рахунку та метод зміни залишку на рахунку зробити приватними. 

Потрібно:

    Створити екземпляр кожного з класу, виконати всі методи.
    здійснити доступ до атрибутів та методів кожного екземпляра (додавши нові методи):

    з свого класу
    з класу , що наслідується 

    з основної програми.
     Чи вірні наступні припущення:

    захищені методи доступні зі свого класу, з класу що наслідується, та з основної програми
    приватні методи доступні зі свого класу, але закриті для доступу з класу що наслідується, та з основної програми. Проте до них можна добратись через нотацію _{origin Class}__method | attr

"""

class Client:
    def __init__(self, pib, data_n):
        self.pib = pib
        self.data_n = data_n    

    def __str__(self):
        return self.pib+ ' ' + self. data_n

    def about_client(self):
        print(self.pib + ' ' + self.data_n)    

class Account:

    def __init__(self, client: Client):
        self.client = client
        self._balance= 0 #protected

    def _change_balance(self, number): #protected method
        self._balance+= number    

    def balance (self):
        print(f'balance = {self._balance}')    

    def change_balance(self, number): #to call protected method
        self._change_balance(number)

class GoldAccount(Account): #inheritance
    
    def __change_balance(self, number):
         self._balance += number    

    def __balance(self):
        print(f'balance = {self._balance}')
    
    def balance_test(self):
        print(f'balance = {self.__balance_test}')
    
    def change_balance(self, number):
        self.__change_balance(numbe)    

    def balance(self):
        return self.__balance()

client = Client('Petrenko', '12.03.2009')
client.about_client()
#print(Client.test)
print(client.about_client)
account  = Account(client)
account.change_balance(10000)
account.balance()

gold_account = GoldAccount(client)
gold_account.change_balance(50000)
# print(gold_account.balance())
# print(gold_account.__dict__)
print(dir(gold_account))
