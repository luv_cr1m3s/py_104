"""
Задача 2.
Як у відео, малювання, лише додати створення 6,8, та багатокутника (задається к-сть сторін).
"""
import turtle

class MyTurtle(turtle.Turtle):

    def draw_nsides(self, n_sides):
        angle = 360//n_sides
        print(angle)
        for _ in range(0,n_sides):
            self.forward(25)
            self.left(angle)

my_turtle = MyTurtle()
my_turtle.draw_nsides(12)
a = input()
