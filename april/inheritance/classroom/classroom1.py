#!/usr/bin/env python3

"""
Задача 2.
Створіть класи, з одним методом, що виводить ім“я цього класу.

    Нехай клас Class_2 наслідується від класу Class_1.
    Клас 3 – від 4.
    Клас 5 – від 6 , а той в свою чергу – від 7 (додайте інший метод).
    Клас 8 наслідується від класів 2, 3, 6.

    Створіть екземпляр класу 2, запустіть його метод, чи коректно відпрацював ?
    Поміняйте порядок наслідування класу 8 на 6, 2, 3 , що змінилось.
    Викличте інший метод, з класу 7, відпрацював ? Зрозуміло чому ?
    Яка функція виводить порядок виконання класів ? (Не було в матеріалах уроку ? А загуглити ? ;) )
"""

"""
Задача 3.
Реалізуйте функцію, щоб виводила mro у форматі:
A -> C -> D -> ...
"""

def custom_mro(obj):
    classname = obj.__class__
    classes = classname.mro()
    
    clear_n = [ i.__name__ for i in classes]
    
    print(*clear_n, sep=" -> ")

class Class_1:

    def __init__(self):
        pass

    def say_my_name(self):
        print(type(self))
    
    def custom_mro(self):
        classname = self.__class__
        classes = classname.mro()
        clear_n = [ i.__name__ for i in classes]
        
        print(*clear_n, sep=" -> ")


class Class_2(Class_1):

    def __init__(self):
        pass

    def other(self):
        print(f"vars: {vars(self)}")

class Class_4:

    def __init__(self):
        pass


class Class_3(Class_4):

    def __init__(self):
        super().__init__()

class Class_7:
    
    def __init__(self):
        pass
    
    def other(self):
        print(vars(self))


class Class_6(Class_7):
    
    def __init__(self):
        super().__init__()  

class Class_5(Class_6):
    
    def __init__(self):
        super().__init__()  

class Class_8(Class_2, Class_6, Class_3):

    def __init__(self):
        Class_2.__init__(self)
        Class_3.__init__(self)
        Class_6.__init__(self)


a = Class_2()
a.say_my_name()

b = Class_8()
b.say_my_name()
b.other()

custom_mro(b)

print("\n-------------------\n")
b.custom_mro()
