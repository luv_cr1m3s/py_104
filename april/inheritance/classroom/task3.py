"""
Задача 3.
Створити клас Detail, з полями назва та вартість.
 Реалізувати додавання екземплярів класу Detail з допомогою __add__(), результат – сума вартостей товарів. Виконувати перевірку, щоб обидва доданки були типу Detail, в іншому випадку – видавати виключення TypeError.
 """
class Detail:

    def __init__(self, name, price):
        self.name = name
        self.price = price

    def __add__(self, other):
        try:
            if not isinstance(other, Detail):
                raise TypeError
        except TypeError:
            print(f"Can't add {type(other)} to {type(self)}")
            exit(1)

        #self.price += other.price
        return self.price + other.price

    def __str__(self):
        return f"{self.name}, with price {self.price}"


a = Detail("arm", 100)
b = Detail("leg", 200)

print(a+b)
print(a)
print(b)

print(a+1)
#print(1+a)
