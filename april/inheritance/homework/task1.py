#!/usr/bin/env python3

"""
Task 1

School

Make a class structure in python representing people at school. Make a base class called Person, a class called Student, and another one called Teacher. Try to find as many methods and attributes as you can which belong to different classes, and keep in mind which are common and which are not. For example, the name should be a Person attribute, while salary should only be available to the teacher.
"""

class Person:

    def __init__(self, name, age, gender):
        self.name = name
        self.age = age
        self.gender = gender

    def wake_up(self, time):
        pass

class Student(Person):

    def __init__(self, name, age, gender, school_class):
        super().__init__(name, age, gender)
        self.school_class = school_class
    
    def prepare_homewor(self):
        pass

    def walk_to_school(self):
        pass


class Teacher(Person):

    def __init__(self, name, age, gender, salary, spec):
        super().__init__(name, age, gender)
        self.salary = salary
        self.spec = spec
    
    def walk_to_school(self):
        pass

    def prepare_program(self):
        pass


p = Person("O", 23, "male")
s = Student("S", 12, "female", "9A")
t = Teacher("P", 44, "male", 13500, "History")
