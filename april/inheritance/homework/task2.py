#!/usr/bin/env python3

"""
Task 2

Mathematician

Implement a class Mathematician which is a helper class for doing math operations on lists

The class doesn't take any attributes and only has methods:

    square_nums (takes a list of integers and returns the list of squares)
    remove_positives (takes a list of integers and returns it without positive numbers
    filter_leaps (takes a list of dates (integers) and removes those that are not 'leap years'
"""

def leap_year(year):
    if (year % 400 == 0) and (year % 100 == 0):
        return True
    elif (year % 4 ==0) and (year % 100 != 0):
        return True
    else:
        return False

class Mathematician:

    def __init__(self):
        pass
    def square_nums(self, num_array):

        result = [i*i for i in num_array]
        return result

    def remove_positives(self, num_array):

        result = [i for i in num_array if i < 0]
        return result

    def filter_leaps(self, num_array):

        result = [i for i in num_array if leap_year(i)]
        return result

m = Mathematician()

assert m.square_nums([7, 11, 5, 4]) == [49, 121, 25, 16]
assert m.remove_positives([26, -11, -8, 13, -90]) == [-11, -8, -90]
assert m.filter_leaps([2001, 1884, 1995, 2003, 2020]) == [1884, 2020]

print("ALL TESTS PASSED")
