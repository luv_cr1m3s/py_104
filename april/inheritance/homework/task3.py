#!/usr/bin/env python3

"""
Task 3

Product Store

Write a class Product that has three attributes:

    type
    name
    price

Then create a class ProductStore, which will have some Products and will operate with all products in the store. All methods, in case they can’t perform its action, should raise ValueError with appropriate error information.

Tips: Use aggregation/composition concepts while implementing the ProductStore class. You can also implement additional classes to operate on a certain type of product, etc.

Also, the ProductStore class must have the following methods:

    add(product, amount) - adds a specified quantity of a single product with a predefined price premium for your store(30 percent)
    set_discount(identifier, percent, identifier_type=’name’) - adds a discount for all products specified by input identifiers (type or name). The discount must be specified in percentage
    sell_product(product_name, amount) - removes a particular amount of products from the store if available, in other case raises an error. It also increments income if the sell_product method succeeds.
    get_income() - returns amount of many earned by ProductStore instance.
    get_all_products() - returns information about all available products in the store.
    get_product_info(product_name) - returns a tuple with product name and amount of items in the store.
"""
"""
products = {"name": {"item":Product, "count": -1}
discounts = {"name/type":percent}
"""

class  Product:

    def __init__(self, ptype, name, price):
        self.ptype = ptype
        self.name = name
        self.price = price
    
    def __str__(self):
        return f"{self.name} of type {self.ptype} wit price {self.price}$"

class ProductStore:

    def __init__(self):
        self.product_list = {}
        self.discounts = {}
        self.income = 0

    def add(self, product, amount):
        try:
            if amount < 0:
                raise ValueError
        except ValueError as e:
            print("Count of products cant be nagtive", amount) 
            return

        if product.name in self.product_list.keys():
            self.product_list[product.name]["count"] += amount
        else:
            self.product_list[product.name] = {"item":product, "count": amount}

    def set_discount(self, identifier, percent, identifier_type='name'):
        if identifier_type == "name":
            self.discounts[identifier] = percent
        else:
            self.discounts[identifier_type] = percent

    def sell(self, product_name, amount):
        tmp = self.product_list[product_name]["count"] - amount
        
        try:
            if tmp < 0:
                raise ValueError
        except ValueError as e:
            print("Don't have enough products: ", amount)
            return

        discount = 1

        if product_name in self.discounts.keys():
            discount = self.discounts[product_name] / 100
        elif pd := self.product_list[product_name]["item"].ptype in self.discounts.keys():
            discount = self.discounts[pd] / 100

        self.product_list[product_name]["count"] -= amount
        income = self.product_list[product_name]["item"].price * amount * discount
        self.income += income
        print(f"Sold {amount} of {self.product_list[product_name]['item'].name} with {discount*100}% discount for {income}$") 

    def get_income(self):
        return self.income

    def get_all_products(self):
        for product in self.product_list.keys():
            p = self.product_list[product]["item"]
            c = self.product_list[product]["count"]
            print(f"{p.name}: {p.ptype} {p.price}, count: {c}")
    
    def get_product_info(self, product_name):
        name = self.product_list[product_name]["item"].name
        amount = self.product_list[product_name]["count"]
        return (name, amount)

p = Product("Sport", "Football T-Shirt", 100)
p2 = Product("Food", "Ramen", 1.5)

s = ProductStore()
s.add(p, 10)
s.add(p, -10)
s.add(p2, 300)

s.get_all_products()
print(s.get_income())

s.sell("Ramen", 10)
print(s.get_income())

s.set_discount("Football T-Shirt", 50)
s.sell("Football T-Shirt", 5)

print(s.get_income())
print(s.get_product_info("Ramen"))
print(s.get_product_info("Football T-Shirt"))

s.sell("Ramen", 100000)

