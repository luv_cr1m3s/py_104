#!/usr/bin/env python3

"""
Task 4

Custom exception

Create your custom exception named `CustomException`, you can inherit from base Exception class, but extend its functionality to log every error message to a file named `logs.txt`. Tips: Use __init__ method to extend functionality for saving messages to file
"""

from datetime import datetime
from pathlib import Path

class CustomException(Exception):

    def __init__(self, msg):
        self.msg = msg
        path = Path("logs.txt")
        
        if not path.is_file():
            open(path, "w").close()

        with open(path, 'a') as f:
            message = str(datetime.now())
            message += "    " + self.msg + "\n"
            f.writelines(message)

try:
    a = "asdqwe69123"
    if "69" in a:
        raise CustomException("found 69 in string")
except CustomException as e:
    print(e)

            
