"""
Task 1

Create a class method named `validate`, which should be called from the `__init__` method to validate parameter email, passed to the constructor. The logic inside the `validate` method could be to check if the passed email parameter is a valid email string.
"""

import re
import unittest

class Email:

    def __init__(self, address):
        self.address = Email.validate(address)
    
    @classmethod
    def validate(cls, address):
        exp = r"[A-Za-z0-9]+[-]?[.]?[_]?[A-Za-z0-9]+@[A-Za-z0-9-]+[.]{1}[A-Z|a-z]{2,7}"
        result = re.fullmatch(exp, address)
        if result:
            return address
        return False
    
    def __str__(self):
        return f"{self.address}"

class TestStringMethods(unittest.TestCase):

    def test_email(self):
        test_cases = {
                "abc-@mail.com": False,
                "abc-d@mail.com": True,
                "abc..def@mail.com": False,
                "abc.def@mail.com": True,
                ".abc@mail.com": False,
                "abc@mail.com": True,
                "abc#def@mail.com": False,
                "abc_def@mail.com": True,
                "abc.def@mail.c": False,
                "abc.def@mail.cc": True,
                "abc.def@mail#archive.com": False,
                "abc.def@mail-archive.com": True,
                "abc.def@mail": False,
                "abc.def@mail.org": True,
                "abc.def@mail..com": False,
                "abc.def@mail.com": True }
    
        for key in test_cases.keys():
            tmp = Email(key)
            print(f"{key : <25} --> {str(test_cases[key]): >5}")
            self.assertEqual(type(tmp.address) == str, test_cases[key])

if __name__ == "__main__":
    #email = Email("abc-@mail.com")
    #print(email.address)
    #email = Email("abc-d@mail.com")
    #print(email.address)
    #email = Email("abc..def@mail.com")
    #print(email.address)

    unittest.main()
