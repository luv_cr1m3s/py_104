"""
Implement 2 classes, the first one is the Boss and the second one is the Worker.

Worker has a property 'boss', and its value must be an instance of Boss.

You can reassign this value, but you should check whether the new value is Boss. Each Boss has a list of his own workers. You should implement a method that allows you to add workers to a Boss. You're not allowed to add instances of Boss class to workers list directly via access to attribute, use getters and setters instead!
"""

class Boss:

    def __init__(self, id_: int, name: str, company: str):
        self.id = id_
        self.name = name
        self.company = company
        self._workers = []
    
    @property
    def workers(self):
        return ", ".join([str(i) for i in self._workers])

    @workers.setter
    def workers(self, worker):
        try:
            if not isinstance(worker, Boss):
                self._workers.append(worker)
            else:
                raise TypeError
        except TypeError:
            print("Can't add instance of Boss to Boss class")

    @workers.deleter
    def workers(self):
        self._workers = []


class Worker:

    def __init__(self, id_: int, name: str, company: str, boss: Boss):
        self.id = id_
        self.name = name
        self.company = company
        self.boss = boss

    @property
    def boss(self):
        return self._boss 
    
    @boss.setter
    def boss(self, boss):
        try:
            if not isinstance(boss, Boss):
                raise TypeError
            self._boss = boss
        except TypeError:
            print(f"Can't assign {type(boss)} to {type(self)}")
    
    def __str__(self):
        return f"{self.name}"

if __name__ == "__main__":
    boss = Boss(123, "Big Boss", "Unicorn")
    worker = Worker(1231, "First", "Unicorn", boss)
    worker1 = Worker(1232, "Second", "Unicorn", 123)

    worker.boss = Boss(13, "Small Boss", "Unicorn")
    print(worker.boss.name)

    boss.workers = worker
    boss.workers = worker1
    print(boss.workers)

    boss.workers = boss
    del boss.workers

    print(boss.workers)
