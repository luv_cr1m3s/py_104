"""
Task 3

Write a class TypeDecorators which has several methods for converting results of functions to a specified type (if it's possible):

methods:

to_int

to_str

to_bool

to_float

"""

import unittest

class TypeDecorators:
    
    @staticmethod
    def to_str(f):
        def inner(*args, **kwargs):
            try:
                return str(f(*args, **kwargs))
            except:
                return None
        return inner

    @staticmethod
    def to_int(f):
        def inner(*args, **kwargs):
            try:
                return int(f(*args, **kwargs))
            except:
                return None
        return inner

    @staticmethod
    def to_bool(f):
        def inner(*args, **kwargs):
            try:
                return bool(f(*args, **kwargs))
            except:
                return None
        return inner

    @staticmethod
    def to_float(f):
        def inner(*args, **kwargs):
            try:
                return float(f(*args, **kwargs))
            except:
                return None
        return inner


class TestDecoratorMethods(unittest.TestCase):

    def test_to_str(self):
        @TypeDecorators.to_str
        def foo(a, b):
            return a ** b

        self.assertEqual(foo(4, 4), "256")
        self.assertEqual(foo([], {}), None)

    def test_to_int(self):
        @TypeDecorators.to_int
        def foo(a, b):
            return a ** b

        self.assertEqual(foo(4.0, 4.0), 256)
        self.assertEqual(foo([], {}), None)

    def test_to_bool(self):
        @TypeDecorators.to_bool
        def foo(a, b):
            return a ** b

        self.assertEqual(foo(4, 4), True)
        self.assertEqual(foo([], {}), None)

    def test_to_float(self):
        @TypeDecorators.to_float
        def foo(a, b):
            return a ** b

        self.assertEqual(foo(4, 4), 256.0)
        self.assertEqual(foo([], {}), None)

if __name__ == "__main__":
    unittest.main()
