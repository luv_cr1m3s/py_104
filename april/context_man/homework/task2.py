#!/usr/bin/env python3



"""
Task 2

Writing tests for context manager

Take your implementation of the context manager class from Task 1 and write tests for it. Try to cover as many use cases as you can, positive ones when a file exists and everything works as designed. And also, write tests when your class raises errors or you have errors in the runtime context suite."""

from io import UnsupportedOperation
from task1 import MyOpen 
import unittest

class TestSum(unittest.TestCase):
    message = "Foo world!"
    filename = "foo.txt"

    def test_open(self):
        f = MyOpen(self.filename, "w+")
        with f:
            f.write(self.message)
            f.read()
        self.assertEqual(f.counter, 2)
        self.assertTrue(f.file.closed)

    def test_read(self):
        test_len = 0
        act_len = len(self.message)

        with MyOpen(self.filename, "r") as f:
            test_len = len(f.read())

        self.assertEqual(test_len, act_len)

    def test_open_fail(self):
        with self.assertRaises(FileNotFoundError):
            with MyOpen("fail.txt", "r") as f:
                f.write("fail")

    def test_read_fail(self):
        with self.assertRaises(UnsupportedOperation):
            with MyOpen(self.filename, "r") as f:
                f.write("fail")



if __name__ == "__main__":
    unittest.main()
