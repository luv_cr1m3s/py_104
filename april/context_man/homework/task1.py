"""
Task 1

File Context Manager class

Create your own class, which can behave like a
built-in function `open`. Also, you need to 
extend its functionality with counter and logging.
Pay special attention to the implementation of 
`__exit__` method, which has to cover all the 
requirements to context managers mentioned here:
"""

from pathlib import Path
import logging
from datetime import datetime

class MyOpen:
    logger = logging.getLogger()
    logging.basicConfig(level=logging.INFO, filename='MyOpen.log')


    def __init__(self, filename, mode):        
        self.filepath = Path(filename)

        if not self.filepath.is_file() and not "w" in mode:
            self.logger.warning(f"{self.__get_time()} --- failed to init {filename} exception: {FileNotFoundError}")
            raise FileNotFoundError
            
        self.mode = mode
        self.counter = 0    
        self.logger.info(f"{self.__get_time()} --- init {filename}")


    def __get_time(self):
        return datetime.now().strftime("***%m:%d--%H:%M:%S.%f***")


    def __enter__(self):
        self.file = self.filepath.open(mode=self.mode)
        self.logger.info(f"{self.__get_time()} --- entering conext manager for {self.filepath} in {self.mode} mode")

        return self.file


    def __exit__(self, e_type, e_value, e_traceback):
        self.file.close()
        self.logger.info(f"{self.__get_time()} --- closing file {self.filepath}")
        if e_type is not None:
            logging.warning(f"{self.__get_time()} --- An exception of type {e_type} occured.")


    def read(self):
        self.counter += 1
        self.logger.info(f"{self.__get_time()} --- reading {self.filepath}")

        return self.file.read()


    def write(self, text):
        self.counter += 1
        self.logger.info(f"{self.__get_time()} --- writing {text} to {self.filepath}")

        return self.file.write(text)
