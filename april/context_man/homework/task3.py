"""
Task 3 (Optional)

Pytest fixtures with context manager

Create a simple function, which performs any logic of your choice with text data, which it obtains from a file object, passed to this function ( def test(file_obj) ). 

Create a test case for this function using pytest library (Full pytest documentation). 

Create pytest fixture, which uses your implementation of the context manager to return a file object, which could be used inside your function.

"""

from task1 import MyOpen
import pytest
import os

def str_man(file_obj):

    content = ""

    with open("madrigal.txt", "r") as f:
        content = f.read()
    
    content = content.replace("Age", "uWu")
    file_obj.write(content)
    file_obj.seek(0)
    
    return file_obj


@pytest.fixture
def file_man_fixture():
    
    with MyOpen("test.txt", "w+") as file:
        yield file

    file.close()
    os.system("rm test.txt")

def test_fixture(file_man_fixture):
    result = str_man(file_man_fixture)
    txt = result.read()    
    assert result.closed == False
    assert txt != ''
