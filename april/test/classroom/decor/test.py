"""
Задача 1.
Виконати тестування функції з 14.3
Write a decorator `arg_rules` that validates arguments passed to the function.....
"""
from decor import arg_rules 
import unittest

@arg_rules(type_=str, max_length=15, contains=['05', '@'])
def slogan(name):
    return f"{name} drinks pepsi in his brand new BMW!"

"""
assert slogan('S@SH05') == 'S@SH05 drinks pepsi in his brand new BMW!'
print("____________________________")
print(slogan('johndoe05@gmail.com'))
print(slogan('S@SH05'))
"""


class TestSum(unittest.TestCase):
	
    def test_basic(self):
        result = slogan('johndoe05@gmail.com')
        self.assertFalse(result)
        result = slogan(123)
        self.assertFalse(result)
        result = slogan('jo@hndomail.com')
        self.assertFalse(result)
        result = slogan('jo05hndoil.com')
        self.assertFalse(result)

    
    def test_output(self):
        self.assertEqual(slogan('S@SH05'), 'S@SH05 drinks pepsi in his brand new BMW!')

    


if __name__ == "__main__":
    unittest.main()
