#!/usr/bin/env python3
"""
Task 2

Library

Write a class structure that implements a library. Classes:

1) Library - name, books = [], authors = []

2) Book - name, year, author (author must be an instance of Author class)

3) Author - name, country, birthday, books = []

Library class

Methods:

- new_book(name: str, year: int, author: Author) - returns an instance of Book class and adds the book to the books list for the current library.

- group_by_author(author: Author) - returns a list of all books grouped by the specified author

- group_by_year(year: int) - returns a list of all the books grouped by the specified year

All 3 classes must have a readable __repr__ and __str__ methods.

Also, the book class should have a class variable which holds the amount of all existing books
"""
class Author:

    def __init__(self, name, country=None, birthday=None, books=[]):
        self.name = name
        self.country = country
        self.birthday = birthday
        self.books = books.copy()

    def add_book(self, book):
        self.books.append(book)

    def __str__(self):
        return f"\nName: {self.name}\nFrom: {self.country}\nBirthday: {self.birthday}\nBooks: {[book.name for book in self.books]}\n"
    
    def __repr__(self):
        return f"{self.name}, {self.country}, {self.birthday}, {self.books}"


class Book:

    def __init__(self, name, year=None, author=None):
        self.name = name
        self.year = year
        if isinstance(author, Author):
            self.author = author
        else:
            self.author = Author(name = author)

    def __str__(self):
        return f"\nBook title: {self.name}\nPublisehed in: {self.year}\nWritten by: {self.author.name}\n"

    def __repr__(self):
        return f"{self.name}, {self.year}, {self.author}"


class Library:

    def __init__(self, name, books=[], authors=[]):
        self.name = name
        self.books = []
        self.books.extend(books)
        self.authors = []
        self.authors.extend(authors)


    def new_book(self, name, year, author):

        new_book = Book(name, year, author)
        self.books.append(new_book)
        self.authors.append(author)
        self.authors = list(set(self.authors))
        return new_book


    def group_by_author(self, author):

        result = []

        for book in self.books:
            if book.author == author:
                result.append(book)

        return result
    
    def group_by_year(self, year):

        result = []

        for book in self.books:
            if book.year == year:
                result.append(book)

        return result

    def __str__(self):
        return f"\n{self.name} library, with {len(self.books)} books, by {len(self.authors)} authors\n" 

    def __repr__(self):
        return f"{self.name}\n{self.books}\n{self.authors}"

