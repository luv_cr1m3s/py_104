#!/usr/bin/env python3

"""
Fraction

Створіть клас Fraction, який буде представляти всю 
базову арифметичну логіку для дробів (+, -, /, *)
з належною перевіркою й обробкою помилок. Потрібно 
додати магічні методи для математичних операцій та операції 
порівняння між об'єктами класу Fraction
 

class Fraction:
    pass

if __name__ == "__main__":
    x = Fraction(1, 2)
    y = Fraction(1, 4)
    x + y == Fraction(3, 4)

"""

import math

class Fraction:

    def __init__(self, numerator, denominator):
        self.numerator = 0
        self.denominator = 0

        try:
            if denominator == 0 and numerator != 0:
                raise ZeroDivisionError
            if type(numerator) != int or type(denominator) != int:
                raise TypeError
            if numerator != 0:
                divisor = math.gcd(numerator, denominator)
                self.numerator = int(numerator/divisor)
                self.denominator = int(denominator/divisor)
                if self.denominator < 0:
                    self.numerator *= -1
                    self.denominator *= -1
        except ZeroDivisionError:
            print(f"Can't create fraction with '0' as denominator")
            raise
        except TypeError as e:
            print(f"Can't create fraction from {type(numerator)} and {type(denominator)}", e)
            print("Use: Fraction(<int>, <int>)")
            raise e


    def __add__(self, other):
        try:
            if not isinstance(other, Fraction):
                raise TypeError
            if other.numerator == 0:
                return self
            if self.numerator == 0:
                return other

            tmp_denom = self.denominator * other.denominator
            tmp_numer = self.numerator*other.denominator + other.numerator*self.denominator
            result = Fraction(tmp_numer, tmp_denom)
            return result
        except TypeError as e:
            print(f"Can't add {type(self)} to {type(other)}")
            raise e
    
    def __sub__(self, other):
        try:
            if not isinstance(other, Fraction):
                raise TypeError
            if other.numerator == 0:
                return self
            if self.numerator == 0:
                return other

            tmp_denom = self.denominator * other.denominator
            tmp_numer = self.numerator*other.denominator - other.numerator*self.denominator
            result = Fraction(tmp_numer, tmp_denom)
            return result
        except TypeError as e:
            print(f"Can't add {type(self)} to {type(other)}")
            raise e
    
    def __mul__(self, other):
        try:
            if not isinstance(other, Fraction):
                raise TypeError

            tmp_denom = self.denominator * other.denominator
            tmp_numer = self.numerator * other.numerator
            result = Fraction(tmp_numer, tmp_denom)
            return result
        except TypeError as e:
            print(f"Can't add {type(self)} to {type(other)}")
            raise e
    
    def __truediv__(self, other):
        try:
            if not isinstance(other, Fraction):
                raise TypeError

            tmp_denom = self.denominator * other.numerator
            tmp_numer = self.numerator * other.denominator
            result = Fraction(tmp_numer, tmp_denom)
            return result
        except TypeError as e:
            print(f"Can't add {type(self)} to {type(other)}")
            raise e

    def __eq__(self, other):
        try:
            if not isinstance(other, Fraction):
                raise TypeError
            if self.numerator == other.numerator:
                if self.denominator == other.denominator:
                    return True
            return False
        except TypeError as e:
            print(f"Can't compare {type(self)} to {type(other)}")
            raise e

    def __lt__(self, other):
        try:
            if not isinstance(other, Fraction):
                raise TypeError
            return self.numerator * other.denominator < other.numerator * self.denominator
        except TypeError as e:
            print(f"Can't compare {type(self)} to {type(other)}")
            raise e

    def __le__(self, other):
        try:
            if not isinstance(other, Fraction):
                raise TypeError
            return self.__lt__(other) or self.__eq__(other) 
        except TypeError as e:
            print(f"Can't compare {type(self)} to {type(other)}")
            raise e


    def __str__(self):
        return f"{self.numerator}/{self.denominator}"


if __name__ == '__main__':

    a1 = Fraction(1, 2)
    a2 = Fraction(2, 1)
    a3 = Fraction(2, 1)
    print(a1 != a2)
    print(a2 != a3)
    print(a1 < a2)
    print(a2 >= a3)
