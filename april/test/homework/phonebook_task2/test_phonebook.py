#!/usr/bin/env python3

"""
Task 1

Pick your solution to one of the exercises in this module. Design tests for this solution and write tests using unittest library.
"""

import unittest
import json
import phone_book

class TestFraction(unittest.TestCase):

    """
        since it was developed as a bunch of function controled by main function
        there are not much what can be tested:
        
        - raising error if wrong file provided
        
        - result of loading db
        
        - result of sorting db
        
        All other functions just return control, so I can't control state or gather data
        to validate it
    """

    def  test_init(self):
        with self.assertRaises(FileNotFoundError):
            phone_book.phonebook("foo.txt")
    
    def test_load(self):
        with self.assertRaises(FileNotFoundError):
            phone_book.load_data("foo.txt")
        
        db = phone_book.load_data("data.json")
        with open("data.json", "r") as f:
            test_db = json.load(f)
        self.assertEqual(db, test_db)

    def test_sort(self):
        db = phone_book.load_data("data.json")

        sorted_db = phone_book.sort_db(db)
        test_db = {key:db[key] for key in sorted(db.keys())}
        self.assertEqual(sorted_db, test_db)


if __name__ == '__main__':
    unittest.main()
