SELECT 
	e.first_name, e.last_name, e.department_id, d.department_name
FROM employees AS e
LEFT JOIN department AS d
ON e.department_id = d.department_id
ORDER BY d.department_name
LIMIT 10;
SELECT 
	e.first_name, e.last_name, d.department_name, l.city, l.state_province
FROM employees AS e
LEFT JOIN department AS d
ON e.department_id = d.department_id
JOIN locations AS l
ON l.location_id = d.location_id
LIMIT 10;
SELECT 
	e.first_name, e.last_name, e.department_id, d.department_name
FROM employees AS e
JOIN department AS d
ON e.department_id = d.department_id
WHERE (d.department_id IS 80 OR d.department_id IS 40 )
ORDER BY d.department_name
LIMIT 3;
SELECT * FROM departments
LIMIT 3;
SELECT 
	e.first_name, e.manager_id, m.first_name 
FROM employees AS e 
JOIN employees AS m 
ON e.manager_id = m.employee_id 
ORDER BY m.first_name
LIMIT 3;
SELECT
	e.first_name, e.last_name, j.job_title, (j.max_salary - e.salary) AS diff
FROM employees AS e
JOIN jobs AS j
ON e.job_id = j.job_id
ORDER BY diff DESC
LIMIT 3;
SELECT
	j.job_title, AVG(e.salary)
FROM jobs AS j
JOIN employees as e
ON e.job_id = j.job_id
GROUP BY j.job_title;
SELECT 
	e.first_name, e.last_name, e.salary, l.city
FROM employees AS e
JOIN department AS d
ON e.department_id = d.department_id
JOIN locations AS l
ON l.location_id = d.location_id
WHERE l.city IS 'London';
SELECT
	d.department_name, COUNT(e.employee_id)
FROM department AS d
JOIN employees AS e
ON e.department_id = d.department_id
GROUP BY d.department_name;

