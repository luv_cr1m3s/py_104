#!/usr/bin/env python3

import unittest

"""
Task 1

Write a program that reads in a sequence of characters and prints them in reverse order, using your implementation of Stack.
"""

class Stack:

    def __init__(self):
        self._stack = []

    def push(self, value):
        self._stack.append(value)

    def pop(self):
        return self._stack.pop()

    def __len__(self):
        return  len(self._stack)

    def isEmpty(self):
        return len(self._stack) == 0
    
    def __str__(self):
        return f"{self._stack}"


def rev_print(string):
    stack = Stack()
    
    for i in string:
        stack.push(i)

    while not stack.isEmpty():
        print(stack.pop(), end='')
    
    print()


"""
Task 2

Write a program that reads in a sequence of characters, and determines whether it's parentheses, braces, and curly brackets are "balanced."
"""

def is_balanced(string):

    stack = Stack()
    chars = { "[":"]", "{":"}", "(":")"}

    for char in string:
        if char in chars.keys():
            stack.push(char)
        else:
            if not stack:
                return False
            tmp = stack.pop()
            if tmp in chars.keys():
                if char != chars[tmp]:
                    return False
    if len(stack) != 0:
        return False
    return True

"""
Task 3

Extend the Stack to include a method called get_from_stack that searches and returns an element e from a stack. Any other element must remain on the stack respecting their order. Consider the case in which the element is not found - raise ValueError with proper info Message

 

Extend the Queue to include a method called get_from_stack that searches and returns an element e from a queue. Any other element must remain in the queue respecting their order. Consider the case in which the element is not found - raise ValueError with proper info Message
"""

class MyStack(Stack):

    def get_from_stack(self, index):
        if index < 0 or index >= len(self._stack):
            raise ValueError
        else:
            value = self._stack[index]
            self._stack = self._stack[:index] + self._stack[index+1:]
            return value


class Queue(MyStack):

    def __init__(self, value=None):
        if value:
            self.queue = [value]
        else:
            self.queue = []

    def enqueue(self, item):
        return self.queue.append(item)

    def dequeue(self):
        return self.queue.pop(0)

    def size(self):
        return len(self.queue)

    def isempty(self):
        return len(self.queue) == 0

    def __str__(self):
        return f'{self.queue}'


class TestIsBalanced(unittest.TestCase):
    def test_is_balanced_with_balanced_input(self):
        self.assertTrue(is_balanced("()"))
        self.assertTrue(is_balanced("[]"))
        self.assertTrue(is_balanced("{}"))
        self.assertTrue(is_balanced("(())"))
        self.assertTrue(is_balanced("[[]]"))
        self.assertTrue(is_balanced("{{}}"))
        self.assertTrue(is_balanced("()[]{}"))
        self.assertTrue(is_balanced("{[()]}"))
        self.assertTrue(is_balanced("(({}))[]"))
        
    def test_is_balanced_with_unbalanced_input(self):
        self.assertFalse(is_balanced("("))
        self.assertFalse(is_balanced(")"))
        self.assertFalse(is_balanced(")("))
        self.assertFalse(is_balanced("({)}"))
        self.assertFalse(is_balanced("[[)]]"))
        self.assertFalse(is_balanced("{{}()}]"))
        
    def test_is_balanced_with_empty_input(self):
        self.assertTrue(is_balanced(""))

    def setUp(self):
        self.stack = MyStack()
        self.stack.push(1)
        self.stack.push(2)
        self.stack.push(3)

        self.queue = Queue()
        self.queue.enqueue(1)
        self.queue.enqueue(2)
        self.queue.enqueue(3)


    def test_get_from_stack_with_valid_index(self):
        self.assertEqual(self.stack.get_from_stack(2), 3)
        self.assertEqual(self.stack.get_from_stack(1), 2)
        self.assertEqual(self.stack.get_from_stack(0), 1)

    def test_get_from_stack_with_invalid_index(self):
        with self.assertRaises(ValueError):
            self.stack.get_from_stack(-1)
        with self.assertRaises(ValueError):
            self.stack.get_from_stack(3)
            
    def test_get_from_queue_with_valid_index(self):
        self.assertEqual(self.stack.get_from_stack(2), 3)
        self.assertEqual(self.stack.get_from_stack(1), 2)
        self.assertEqual(self.stack.get_from_stack(0), 1)

    def test_get_from_queue_with_invalid_index(self):
        with self.assertRaises(ValueError):
            self.stack.get_from_stack(-1)
        with self.assertRaises(ValueError):
            self.stack.get_from_stack(3)


if __name__ == "__main__":
    a = input("Enter string to be reversed: ")
    print(a)
    rev_print(a)

    unittest.main()
