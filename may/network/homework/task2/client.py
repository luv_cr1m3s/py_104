import socket

HOST = '127.0.0.1'  # The server's hostname or IP address
PORT = 65432        # The port used by the server


request = input("Enter text and key in format: {text}:{key} --> ")
    
with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    s.connect((HOST, PORT))
    s.sendall(request.encode('ascii'))
    data = s.recv(1024)

print('Received\n', repr(data))
