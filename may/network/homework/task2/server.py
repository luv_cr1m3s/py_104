import socket
import string
import sys

def caesar(text,s):
    result = ""
    
    if text == '':
        return "Nan"
    if s == 0:
        return text

    for i in range(len(text)):
        char = text[i]
 
        if (char.isupper()):
            result += chr((ord(char) + s-65) % 26 + 65)
        else:
            result += chr((ord(char) + s - 97) % 26 + 97)
 
    return result


# Create a TCP/IP socket
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# Bind the socket to the port
server_address = ('localhost', 65432)
print('starting up on {} port {}'.format(*server_address))
print('To exit send "quit" from client')
sock.bind(server_address)

# Listen for incoming connections
sock.listen(1)

while True:
    # Wait for a connection
    print('waiting for a connection')
    connection, client_address = sock.accept()
    try:
        print('connection from', client_address)

        # Receive the data in small chunks and retransmit it
        while True:
            data = connection.recv(1024).decode('utf-8')
            
            if data == "quit":
                print("Stoping the server")
                connection.close()
                sys.exit(0)

            if ":" in data:
                print('received {!r}'.format(data))
                text, key = data.split(":")
                key = int(key)
                cipher = caesar(text, key)
                print('sending data back to the client')
                connection.sendall(cipher.encode('ascii'))
            else:
                print('no data from', client_address)
                break

    finally:
        # Clean up the connection
        connection.close()
