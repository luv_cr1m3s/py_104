import socket

# Create a TCP/IP socket
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

# Bind the socket to the port
server_address = ('localhost', 65432)
print('starting up on {} port {}'.format(*server_address))
sock.bind(server_address)

# Listen for incoming connections

while True:
    # Wait for a connection
    print('waiting for a connection')
    data, addr = sock.recvfrom(1024)
    print('connection from', addr)

    if data:
        print("\n" + "-"*30)

        print(f"data received over UDP from: {addr}")
        print(f"received data: {data}")
        
        print("\n" + "-"*30)
        
