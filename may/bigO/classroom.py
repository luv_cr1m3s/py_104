#!/usr/bin/env python3


"""
адача 1.
 Self Check
 Write two Python functions to find the minimum number in a list. The first function should compare each number to every other number on the list O(n*n)
 . The second function should be linear O(n) (edited)Заміряйте час на виконання двох задач. На масивах випадкових значень:
 10 елементів в масиві
 100
 1000
 """
import random
from functools import wraps
import time

def timeit(func):
    @wraps(func)
    def timeit_wrapper(*args, **kwargs):
        start_time = time.perf_counter()
        result = func(*args, **kwargs)
        end_time = time.perf_counter()
        total_time = end_time - start_time
        print(f'Function {func.__name__} Took {total_time:.4f} seconds result: {result}--> {min(*args)}')
        return result
    return timeit_wrapper

        


@timeit
def my_min(list1):
    result = list1[0]

    for i in list1:
        for j in list1:
            if i < j and i < result:
                result = i
    return result

@timeit
def lin_min(list1):
    result = 99999

    for i in list1:
        if i < result:
            result = i

    return result

list_ten = [random.randint(1, 1001) for _ in range(1001)]
list_hun = [random.randint(1, 10001) for _ in range(10001)]
#list_th = [random.randint(1, 10001) for _ in range(100001)]




my_min(list_ten)
my_min(list_hun)
#my_min(list_th)

lin_min(list_ten)
lin_min(list_hun)
#lin_min(list_th)


