#!/usr/bin/env python3

import requests
import json
import sys
import os

env_api_name = "WEATHER_API"

if len(sys.argv) == 2:
    city = sys.argv[1]
    limit = 1
    api = ''

    if env_api_name in os.environ:
        api = os.environ[env_api_name]
    else:
        print("You need to provide api key as env var:")
        print(f'extern {env_api_name}="[your_key]"')
        sys.exit(-1)

    geo_req = f"http://api.openweathermap.org/geo/1.0/direct?q={city}&limit={limit}&appid={api}"
    geo = requests.get(geo_req)
    
    if geo.status_code == 200:
        data = json.loads(geo.text)[0]
        lon, lat = data["lon"], data["lat"]
        
        weather_req = f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={api}&units=metric"
        weather = requests.get(weather_req)

        if weather.status_code == 200:
            current = weather.json()
            
            current_w = current["weather"][0]

            for key in current_w.keys():
                print(f"{key} --> {current_w[key]}")
            for key in current["main"].keys():
                print(f"{key} --> {current['main'][key]}")
        else:
            print("failed to get weather")
    else:
        print(f"Failed to find {city}")
else:
    print("Usage: ./task3.py {city_name}")
    print(f"You should have https://openweathermap.org api key stored as env variable as {env_api_name}")
