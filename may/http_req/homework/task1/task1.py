#!/usr/bin/env python3

"""
Task 1

Robots.txt

Download and save to file robots.txt from wikipedia, twitter websites etc.
"""

import requests
import sys

if len(sys.argv) == 2:
    req = requests.get(f"https://{sys.argv[1]}/robots.txt")
    if req.status_code == 200:
        with open('robots.txt', 'w') as file:
            file.writelines(f"{req.url!r}\n\n")
            file.writelines(f"{req.headers!r}\n\n")
            file.writelines(f"{req.text}\n\n")
else:
    print("Usage: ./task1.py http://{page_address}")
