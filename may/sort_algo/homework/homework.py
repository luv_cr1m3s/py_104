#!/usr/bin/env python3

import unittest
from time import perf_counter
from random import shuffle

"""
Task 1

A bubble sort can be modified to “bubble” in both directions. The first pass moves “up” the list and the second pass moves “down.” This alternating pattern continues until no more passes are necessary. Implement this variation and describe under what circumstances it might be appropriate.

"""

def cocktail_sort(array):
    start = 0
    end = len(array) - 1
    swapped = True

    while swapped is True:
        swapped = False

        for i in range(start, end):
            if array[i] > array[i + 1]:
                array[i], array[i+1] = array[i+1], array[i]
                swapped = True
        if swapped is False:
            break
        
        end -= 1
        swapped = False

        for i in range(end - 1, start - 1, -1):
            if array[i] > array[i + 1]:
                array[i], array[i + 1] = array[i + 1], array[i]
                swapped = True

        start += 1

    return array


"""
Task 2

Implement the mergeSort function without using the slice operator.
"""

def merge(array, start, mid, end):
    
    start2 = mid + 1

    if array[mid] <= array[start2]:
        return 

    while start <= mid and  start2 <= end:
        if array[start] <= array[start2]:
            start += 1
        else:
            value = array[start2]
            index = start2

            while index != start:
                array[index] = array[index - 1]
                index -= 1

            array[start] = value

            start += 1
            mid += 1
            start2 += 1


def mergeSort(array, start = 0, end = None):

    if end is None:
        end = len(array) -1
    

    if start < end:
        mid = start + (end - start) // 2

        mergeSort(array, start, mid)
        mergeSort(array, mid + 1, end)

        merge(array, start, mid, end)

"""
One way to improve the quicksort is to use an insertion sort on lists that are small in length (call it the “partition limit”). Why does this make sense? Re-implement the quicksort and use it to sort a random list of integers. Perform analysis using different list sizes for the partition limit.
"""
def quicksort(arr, start=0, end=None, partition_limit=10):
    if end is None:
        end = len(arr) - 1

    if start < end:
        if end - start < partition_limit:
            insertion_sort(arr, start, end)
        else:
            pivot = partition(arr, start, end)
            quicksort(arr, start, pivot-1, partition_limit)
            quicksort(arr, pivot+1, end, partition_limit)


def partition(arr, start, end):
    pivot = arr[end]
    i = start - 1
    for j in range(start, end):
        if arr[j] <= pivot:
            i += 1
            arr[i], arr[j] = arr[j], arr[i]
    arr[i+1], arr[end] = arr[end], arr[i+1]
    return i+1


def insertion_sort(arr, start, end):
    for i in range(start+1, end+1):
        key = arr[i]
        j = i-1
        while j >= start and arr[j] > key:
            arr[j+1] = arr[j]
            j -= 1
        arr[j+1] = key


class TestSortingMethods(unittest.TestCase):


    def test_cocatail(self):
        arr = [i for i in range(1, 1000)]
        test = [i for i in range(1, 1000)]
        
        self.assertEqual(cocktail_sort(test), arr)


    def test_merge(self):
        arr = [i for i in range(1, 100)]
        test = [i for i in range(1, 100)]
        shuffle(test)
        self.assertNotEqual(test, arr)
        mergeSort(test)

        self.assertEqual(test, arr)


    def test_quick(self):
        arr = [i for i in range(1, 100)]
        test = [i for i in range(1, 100)]
        shuffle(test)
        self.assertNotEqual(test, arr)
        quicksort(test)

        self.assertEqual(test, arr)




if __name__ == '__main__':
    
    arr = [i for i in range(1, 10000)]
    shuffle(arr)

    for i in [10, 50, 100, 1000, 10000]:
        bench_arr = arr.copy()
        start = perf_counter()
        quicksort(bench_arr, partition_limit=i)
        end = perf_counter()
        print(f"partition_limit = {i}, time: {end - start}")

    unittest.main()
