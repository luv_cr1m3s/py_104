#!/usr/bin/env python3
import unittest
"""
Task 1

from typing import Optional
def to_power(x: Optional[int, float], exp: int) -> Optional[int, float]:
    
    Returns  x ^ exp

    >>> to_power(2, 3) == 8
    True

    >>> to_power(3.5, 2) == 12.25
    True

    >>> to_power(2, -1)
    ValueError: This function works only with exp > 0.

"""

def to_power(x, exp):
    if exp < 0:
        raise ValueError("ValueError: This function works only with exp > 0.")


    if exp == 0:
        return 1

    if exp == 1:
        return x

    return x * to_power(x, exp-1)

"""
Task 2

from typing import Optional
def is_palindrome(looking_str: str, index: int = 0) -> bool:
    Checks if input string is Palindrome
    >>> is_palindrome('mom')
    True

    >>> is_palindrome('sassas')
    True

    >>> is_palindrome('o')
    True
    pass
"""

def is_palindrome(string):
    len_str = len(string) - 1

    if len_str <= 1:
        return True

    if string[0] != string[len_str]:
        return False

    return is_palindrome(string[1:len_str])

"""
Task 3

from typing import Optional
def mult(a: int, n: int) -> int:
    This function works only with positive integers

    >>> mult(2, 4) == 8
    True

    >>> mult(2, 0) == 0
    True

    >>> mult(2, -4)
    ValueError("This function works only with postive integers")
"""

def mult(x, y):

    if x < 0 or y < 0:
        raise ValueError("This function works only with postive integers")

    if x == 0 or y == 0:
        return 0

    if x == 1 or y == 1:
        return x * y

    return x + mult(x, y - 1)


"""
Task 4

def reverse(input_str: str) -> str:
    Function returns reversed input string
    >>> reverse("hello") == "olleh"
    True
    >>> reverse("o") == "o"
    True
"""

def reverse(string):
    len_str = len(string)

    if len_str == 0:
        return ""

    if len_str == 1:
        return string

    return string[len_str-1] + reverse(string[:len_str-1]) 

"""
Task 5

def sum_of_digits(digit_string: str) -> int:
    >>> sum_of_digits('26') == 8
    True

    >>> sum_of_digits('test')
    ValueError("input string must be digit string")
"""

def sum_of_digits(x):

    if not x.isnumeric():
        raise ValueError("input string must be digit string")
    
    len_str = len(x) - 1

    if len_str == 0:
        return int(x)

    return int(x[0]) + sum_of_digits(x[1:]) 


class TestHomework(unittest.TestCase):


    def test_recursion(self):
        self.assertEqual(to_power(2, 3), 8)
        self.assertEqual(to_power(2, 0), 1)
        self.assertEqual(to_power(3, 4), 81)
        self.assertEqual(to_power(5, 2), 25)
        self.assertEqual(to_power(0, 5), 0)

        with self.assertRaises(ValueError):
            to_power(4, -1)
        
        with self.assertRaises(ValueError):
            to_power(2, -2)


    def test_palindrome(self):
        self.assertTrue(is_palindrome("racecar"))
        self.assertTrue(is_palindrome("deified"))
        self.assertTrue(is_palindrome("level"))
        self.assertTrue(is_palindrome("radar"))
        self.assertFalse(is_palindrome("python"))
        self.assertFalse(is_palindrome("hello"))
        self.assertFalse(is_palindrome("world"))


    def test_mult(self):
        self.assertEqual(mult(2, 3), 6)
        self.assertEqual(mult(0, 5), 0)
        self.assertEqual(mult(1, 100), 100)

        with self.assertRaises(ValueError):
            mult(10, -1)


    def test_reverse_string(self):
        self.assertEqual(reverse("hello"), "olleh")
        self.assertEqual(reverse("Python"), "nohtyP")
        self.assertEqual(reverse(""), "")
        self.assertEqual(reverse("a"), "a")
        self.assertEqual(reverse("racecar"), "racecar")


    def test_sum_of_digits(self):
        self.assertEqual(sum_of_digits("123"), 6)
        self.assertEqual(sum_of_digits("456"), 15)
        self.assertEqual(sum_of_digits("123"), 6)
        self.assertEqual(sum_of_digits("0"), 0)
        self.assertEqual(sum_of_digits("100000000000"), 1)

        with self.assertRaises(ValueError):
            self.assertEqual(sum_of_digits("abc"), 0)

        with self.assertRaises(ValueError):
            self.assertEqual(sum_of_digits(""), 0)


if __name__ == '__main__':
    unittest.main()
