#!/usr/bin/env python3

from random import randint

class BinaryTree:
    def __init__(self,rootObj):
        self.key = rootObj
        self.leftChild = None
        self.rightChild = None

    def insertLeft(self,newNode):
        if self.leftChild == None:
            self.leftChild = BinaryTree(newNode)
        else:
            t = BinaryTree(newNode)
            t.leftChild = self.leftChild
            self.leftChild = t

    def insertRight(self,newNode):
        if self.rightChild == None:
            self.rightChild = BinaryTree(newNode)
        else:
            t = BinaryTree(newNode)
            t.rightChild = self.rightChild
            self.rightChild = t

    def getRightChild(self):
        return self.rightChild

    def getLeftChild(self):
        return self.leftChild

    def setRootVal(self,obj):
        self.key = obj

    def getRootVal(self):
        return self.key
    
    def traversal(self, result = []):

        if self is None:
            return result

        result.append(self.key)

        if self.leftChild:
            self.leftChild.traversal(result)
        if self.rightChild:
            self.rightChild.traversal(result)

        return result



    def pre_order(self, tabs = 0) -> None:
        print(tabs * "\t", self.key)
        if self.leftChild:
            self.leftChild.pre_order(tabs+1)
        if self.rightChild:
            self.rightChild.pre_order(tabs+1)

    def post_order(self) -> None:
        if self.leftChild:
            self.leftChild.post_order()
        if self.rightChild:
            self.rightChild.post_order()
        print(self.key, ' -> ', end='')

    def in_order(self) -> None:
        if self.leftChild:
            self.leftChild.in_order()
        print(self.key, ' -> ', end='')
        if self.rightChild:
            self.rightChild.in_order()

    """
Програма мінімум:

Розширити структуру, яку побудували на уроці, можливістю вставки дерева в наявне дерево та видалення піддерева з дерева, що існує.

    """


    def print_tree(self, level = 0):
    
        if self != None:
            if self.leftChild:
                self.leftChild.print_tree(level + 1)
            print(" " * 4 * level + "-> " + str(self.key))
            
            if self.rightChild:
                self.rightChild.print_tree(level + 1)


    def free_node(self, result = []):
        
        if self is None:
            return result
        
        result.append(self)
        if self.leftChild:
            self.leftChild.free_node(result)
        if self.rightChild:
            self.rightChild.free_node(result)

        return result


    def insert_tree(self, tree):

        insertion = self.free_node()
        uniq_ins = list(set(insertion))
        ins_list = []

        for i in uniq_ins:
            if i.leftChild is None or i.rightChild is None:
                ins_list.append(i)

        count = 0

        for i in ins_list:
            print(f"{i.key}: to insert tree use index: {count}")
            count += 1
        
        index = int(input("Enter place where to insert tree :"))
        
        if ins_list[index].leftChild is None:
            ins_list[index].leftChild = tree
        elif ins_list[index].rightChild is None:
            ins_list[index].rightChild = tree
        else:
            print("You are trying toinsert into node that have both children.")


    def delete_tree(self):
        insertion = self.free_node([])
        uniq_ins = list(set(insertion))
        count = 0

        for i in uniq_ins:
            print(f"{i.key} to delete use index: {count}")
            count += 1
        
        i = int(input("Enter index: "))
        
        self.clear_subtree(uniq_ins[i])
        

    def clear_subtree(self, tree):

        if self:

            if self.leftChild is tree:
                self.leftChild = None
                return
            if self.rightChild is tree:
                self.rightChild = None
                return
            
            if self.leftChild:
                self.leftChild.clear_subtree(tree)
            if self.rightChild:
                self.rightChild.clear_subtree(tree)


def gen_tree():
    a = randint(1, 100)
    b = randint(1, 100)
    c = randint(1, 100)
    
    result = BinaryTree(a)
    result.insertLeft(b)
    result.insertRight(c)

    return result

root = BinaryTree(1)
print(root.traversal())
root.print_tree()

for i in range(5):
    root.insert_tree(gen_tree())
    root.print_tree()

for i in range(3):
    root.delete_tree()
    root.print_tree()


