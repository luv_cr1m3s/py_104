CREATE TABLE IF NOT EXISTS projects ( id integer PRIMARY KEY, name TEXT NOT NULL, begin_date TEXT, end_date text);
ALTER TABLE projects RENAME TO sql_hw;
INSERT INTO sql_hw (name,begin_date,end_date) VALUES ('Cool App with SQLite & Python', '2015-01-01', '2015-01-30');
INSERT INTO sql_hw (name,begin_date,end_date) VALUES ('SQLite & Python', '2023-01-01', '2023-01-30');
UPDATE sql_hw SET name = 'Beetroot Python', begin_date = '2023-03-01' WHERE id = 1;
UPDATE sql_hw SET end_date = '2023-06-25' WHERE id = 1;
DELETE FROM sql_hw WHERE id  = 2;
