#!/usr/bin/env python3

"""
As a solution to HW, create a file named: task2.sql with all SQL queries:

    write a query to display the names (first_name, last_name) using alias name "First Name", "Last Name" from the table of employees;
    write a query to get the unique department ID from the employee table
    write a query to get all employee details from the employee table ordered by first name, descending
    write a query to get the names (first_name, last_name), salary, PF of all the employees (PF is calculated as 12% of salary)
    write a query to get the maximum and minimum salary from the employees table
    write a query to get a monthly salary (round 2 decimal places) of each and every employee
"""

from os import readlink
import sqlite3
from sqlite3 import Error

if __name__ == '__main__':

    db_name = "task2.db"

    db = sqlite3.connect(db_name)
    cursor = db.cursor()

    requests = []

    with open("task2.sql", "r") as file:
        requests = file.readlines()

    for request in requests:
        border = len(request) * "+"
        with db:
            print(border)
            print(request, end = '')
            print(border)

            cur = cursor.execute(request)
            for line in cur.fetchall():
                print(line)
             
            print()
           

    db.close()
