#!/usr/bin/env python3

"""
Create a table

Create a table of your choice inside the sample SQLite database, rename it, and add a new column. Insert a couple rows inside your table. Also, perform UPDATE and DELETE statements on inserted rows.

As a solution to this task, create a file named: task1.sql, with all the SQL statements you have used to accomplish this task
"""

import sqlite3
from sqlite3 import Error

if __name__ == '__main__':
    db_name = "task1.db"

    db = sqlite3.connect(db_name)
    cursor = db.cursor()

    requests = []

    with open("task1.sql", "r") as reqs:
        requests = reqs.readlines()
    
    for request in requests:
        with db:
            cur = cursor.execute(request)
            try:
                content = cursor.execute("SELECT * FROM sql_hw")
                for cont in content:
                    print(cont)
                print("+"*40)
            except Error as e:
                print(e)

    cursor.execute("DROP TABLE IF EXISTS sql_hw")

    content = []
    cursor.execute("SELECT * FROM sqlite_master")
    content = cursor.fetchall()

    print(content)

    db.close()
