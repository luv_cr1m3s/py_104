#!/usr/bin/env python3

"""
Реалізувати алгоритм бінарного пошуку за допомогою рекурсії.
"""
import unittest

def binary_search(arr,  value, bot = 0, top = None ):
    if top is None:
        top = len(arr)-1
    
    if bot > top:
        return -1
    
    mid = (top + bot)
    if mid % 2 == 1:
        mid += 1

    mid = mid // 2

    if arr[mid] == value:
        return mid

    if arr[mid] > value:
        return binary_search(arr, value, bot, mid - 1)
    else:
        return binary_search(arr, value, mid + 1, top)

# складність бінарного пошуку і пошуку фібоначі однакові O(nlogn) у найгіршому випадку
# і O(1) в найкращому вони відрізняються тільки відрізками на котрі ділиться список

def fibonacci_search(arr, x):
    fib_m_minus_2 = 0
    fib_m_minus_1 = 1
    fib_m = fib_m_minus_1 + fib_m_minus_2

    while fib_m < len(arr):
        fib_m_minus_2 = fib_m_minus_1
        fib_m_minus_1 = fib_m
        fib_m = fib_m_minus_1 + fib_m_minus_2

    offset = -1

    while fib_m > 1:
        i = min(offset + fib_m_minus_2, len(arr) - 1)

        if arr[i] < x:
            fib_m = fib_m_minus_1
            fib_m_minus_1 = fib_m_minus_2
            fib_m_minus_2 = fib_m - fib_m_minus_1
            offset = i
        elif arr[i] > x:
            fib_m = fib_m_minus_2
            fib_m_minus_1 = fib_m_minus_1 - fib_m_minus_2
            fib_m_minus_2 = fib_m - fib_m_minus_1
        else:
            return i

    if fib_m_minus_1 and arr[offset+1] == x:
        return offset+1

    return -1


class TestBinarySearch(unittest.TestCase):
    def test_binary_search_with_empty_list(self):
        self.assertEqual(binary_search([], 4), -1)
        
    def test_binary_search_with_single_item_list(self):
        self.assertEqual(binary_search([5], 5), 0)
        self.assertEqual(binary_search([5], 2), -1)
        
    def test_binary_search_with_even_length_list(self):
        self.assertEqual(binary_search([1, 3, 5, 7], 5), 2)
        self.assertEqual(binary_search([1, 3, 5, 7], 2), -1)
        self.assertEqual(binary_search([1, 3, 5, 7], 6), -1)
        
    def test_binary_search_with_odd_length_list(self):
        self.assertEqual(binary_search([1, 3, 5, 7, 9], 5), 2)
        self.assertEqual(binary_search([1, 3, 5, 7, 9], 2), -1)
        self.assertEqual(binary_search([1, 3, 5, 7, 9], 6), -1)
        
    def test_binary_search_with_repeated_items(self):
        self.assertEqual(binary_search([1, 2, 2, 3, 4, 5], 2), 1)
        self.assertEqual(binary_search([1, 2, 2, 3, 4, 5], 6), -1)


    def test_fibonacci_search(self):
        arr = [1, 3, 5, 7, 9, 11, 13]
        self.assertEqual(fibonacci_search(arr, 5), 2)
        self.assertEqual(fibonacci_search(arr, 12), -1)

        arr = [0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144]
        self.assertEqual(fibonacci_search(arr, 13), 7)
        self.assertEqual(fibonacci_search(arr, 55), 10)


if __name__ == '__main__':
    unittest.main()

