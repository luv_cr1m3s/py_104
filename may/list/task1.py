#!/usr/bin/env python3

"""
Task 1

Extend UnorderedList

Implement append, index, pop, insert methods for UnorderedList. Also implement a slice method, which will take two parameters `start` and `stop`, and return a copy of the list starting at the position and going up to but not including the stop position.

"""

import unittest

class Node:

    __slots__ = ["_next", "_data"]

    def __init__(self, data):
        self._data = data
        self._next = None

    def get_data(self):
        return self._data

    def get_next(self):
        return self._next

    def set_data(self, data):
        self._data = data

    def set_next(self, new_next):
        self._next = new_next


class UnorderedList:

    def __init__(self):
        self._head = None

    def is_empty(self):
        return self._head is None

    def add(self, item):
        temp = Node(item)
        temp.set_next(self._head)
        self._head = temp
    
    def append(self, value):
        if self._head is None:
            self._head = Node(value)
            return

        node = Node(value)
        insertion = self._head

        while insertion._next:
            insertion = insertion._next
        insertion._next = node
    
    def pop(self):
        if self._head is None:
            return None

        if self._head._next is None:
            result = self._head._data
            self._head = None
            return result

        remove = self._head

        while remove._next._next is not None:
            remove = remove._next

        result = remove._next._data
        remove._next = None

        return result


    def insert(self, value, index):
        node  = Node(value)
        current = self._head
        start = 0

        while start < index and current:
            current = current._next
            start += 1

        node._next = current._next
        current._next = node

    def slice(self, start, end):

        result = []
        index = 0
        current = self._head

        if end <= start:
            return result
        
        while index < end and current:
            if index >= start:
                result.append(current._data)
            index += 1
            current = current._next

        return result

    def size(self):
        current = self._head
        count = 0
        while current is not None:
            count += 1
            current = current.get_next()

        return count

    def search(self, item):
        current = self._head
        found = False
        while current is not None and not found:
            if current.get_data() == item:
                found = True
            else:
                current = current.get_next()

        return found

    def remove(self, item):
        current = self._head
        previous = None
        found = False
        while not found:
            if current.get_data() == item:
                found = True
            else:
                previous = current
                current = current.get_next()

        if previous is None:
            self._head = current.get_next()
        else:
            previous.set_next(current.get_next())

    def __repr__(self):
        representation = "<UnorderedList: "
        current = self._head
        while current is not None:
            representation += f"{current.get_data()} "
            current = current.get_next()
        return representation + ">"

    def __str__(self):
        return self.__repr__()


class TestDataStructures(unittest.TestCase):
    
    def setUp(self):
        self.ulist = UnorderedList()
        self.ulist.append(1)
        self.ulist.append(2)
        self.ulist.append(4)
        self.ulist.append(5)
        self.ulist.append(6)
        self.ulist.append(7)
        self.ulist.append(8)


    def test_pop(self):
        self.assertEqual(self.ulist.pop(), 8)
        self.assertEqual(self.ulist.pop(), 7)

    def test_insert(self):
        self.ulist.insert(69, 0)
        self.ulist.insert(109, 5)
        
        self.assertEqual("<UnorderedList: 1 69 2 4 5 6 109 7 8 >", self.ulist.__repr__())

    def test_slice(self):
        print(self.ulist)

        self.assertEqual(self.ulist.slice(0, 1), [1])
        self.assertEqual(self.ulist.slice(2, 5), [ 4, 5, 6])
        self.assertEqual(self.ulist.slice(2, 0), [])

if __name__ == '__main__':
    unittest.main()
