#!/usr/bin/env python3

import unittest
from task1 import UnorderedList

"""
Task 2

Implement a stack using a singly linked list.

"""

class Stack:

    def __init__(self):
        self.head = UnorderedList()


    def push(self, value):
        self.head.append(value)


    def pop(self):
        return self.head.pop()

"""
Task 3

Implement a queue using a singly linked list.
"""

class Queue(Stack):
    
    def pop(self):
        if self.head._head is None:
            return None

        result = self.head._head.get_data()
        self.head._head = self.head._head._next
        return result

class TestDataStructures(unittest.TestCase):
    
    def test_stack(self):
        stack = Stack()
        stack.push(1)
        stack.push(2)
        stack.push(3)
        stack.push(4)
        stack.push(5)
        stack.push(6)
        
        self.assertEqual(stack.pop(), 6)
        self.assertEqual(stack.pop(), 5)
        self.assertEqual(stack.pop(), 4)
        self.assertEqual(stack.pop(), 3)
        self.assertEqual(stack.pop(), 2)
        self.assertEqual(stack.pop(), 1)
        self.assertEqual(stack.pop(), None)
        
        stack.push(17)
        self.assertEqual(stack.pop(), 17)


    def test_queue(self):
        queue = Queue()
        queue.push(1)
        queue.push(2)
        queue.push(3)
        queue.push(4)
        queue.push(5)
        queue.push(6)

        self.assertEqual(queue.pop(), 1)
        self.assertEqual(queue.pop(), 2)
        self.assertEqual(queue.pop(), 3)
        self.assertEqual(queue.pop(), 4)
        self.assertEqual(queue.pop(), 5)
        self.assertEqual(queue.pop(), 6)
        self.assertEqual(queue.pop(), None)
        
        queue.push(17)
        self.assertEqual(queue.pop(), 17)

if __name__ == '__main__':
    unittest.main()
