import asyncio
import aiohttp
import time

"""
Task 2

Requests using asyncio and aiohttp
"""

async def scrap_page(session, url):
    async with session.get(url) as resp:
        if resp.status == 200:
            return await resp.json()


async def main():

    start = time.perf_counter()
    
    currency = ["GBP", "EUR", "USD"]
    coro_objs = []

    async with aiohttp.ClientSession() as session:
        for c in currency:
            req = f"https://bank.gov.ua/NBUStatService/v1/statdirectory/exchangenew?json&valcode={c}&date=20220224"
            coro_objs.append(scrap_page(session, req))

        results = await asyncio.gather(*coro_objs)
    
    end = time.perf_counter() - start

    print(f"{len(currency)} requests took: {end} seconds")
    for c, resp  in zip(currency, results):
        print(f"{c} : {resp}")

asyncio.run(main())

