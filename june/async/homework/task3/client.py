#!/usr/bin/env python3

import socket

HOST = "127.0.0.1"
PORT = 8000

sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
sock.connect((HOST, PORT))
message = input("Enter message: ")

sock.send(message.encode())
data = sock.recv(1024)

print(f"Received: {data}")

sock.close()
