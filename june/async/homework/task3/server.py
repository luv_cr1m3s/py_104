"""
Task 3

Echo server with asyncio

Create a socket echo server which handles each connection using asyncio Tasks.
"""

import asyncio
import socket

async def echo(conn, loop):
    while data := await loop.sock_recv(conn, 1024):
        res = data.decode('utf-8')
        res = res[::-1]
        print(f"Sending changed data back to  {conn}")
        await loop.sock_sendall(conn, res.encode('utf-8'))

async def listen_for_connection(socket, loop):
    while True:
        conn, addr = await loop.sock_accept(socket)
        conn.setblocking(False)
        print(f"Connection from: {addr}")
        asyncio.create_task(echo(conn, loop))


async def main():
    serv_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    serv_socket.setblocking(False)

    addr = ('localhost', 8000)
    serv_socket.bind(addr)
    serv_socket.listen()

    await listen_for_connection(serv_socket, asyncio.get_event_loop())

asyncio.run(main())

