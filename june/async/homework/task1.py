"""
Task 1

Practice asynchronous code

Create a separate asynchronous code to calculate Fibonacci, factorial, squares and cubic for an input number. Schedule the execution of this code using asyncio.gather for a list of integers from 1 to 10. You need to get four lists of results from corresponding functions.

Rewrite the code to use simple functions to get the same results but using a multiprocessing library. Time the execution of both realizations, explore the results, what realization is more effective, why did you get a result like this.
"""

import asyncio
import multiprocessing
import time

SIZE = 11

async def fibo(n):
    a = 0
    b = 1

    result = 0

    while n > 1:
        result = a + b
        a = b
        b = result
        n -= 1

    return result

async def fact(n):
    result = 1

    while n > 1:
        result *= n
        n -= 1

    return result

async def square(n):
    return n ** 2

async def cube(n):
    return n ** 3

async def main():
        
    fibo_objs = []
    fact_objs = []
    square_objs = []
    cube_objs = []

    for i in range(1, SIZE):
        fibo_objs.append(fibo(i))
        fact_objs.append(fact(i))
        square_objs.append(square(i))
        cube_objs.append(cube(i))

    fibo_res = await asyncio.gather(*fibo_objs)
    fact_res = await asyncio.gather(*fact_objs)
    square_res = await asyncio.gather(*square_objs)
    cube_res = await asyncio.gather(*cube_objs)
    
    print(f"Results: \n\t{fibo_res}\n\t{fact_res}")
    print(f"Results: \n\t{square_res}\n\t{cube_res}")

start = time.perf_counter()

asyncio.run(main())
duration = time.perf_counter() - start
print(f"\nWith async took {duration} seconds\n")

def fibo_m(n):
    a = 0
    b = 1

    result = 0

    while n > 1:
        result = a + b
        a = b
        b = result
        n -= 1

    return result

def fact_m(n):
    result = 1

    while n > 1:
        result *= n
        n -= 1

    return result

def square_m(n):
    return n ** 2

def cube_m(n):
    return n ** 3


def worker(procnum, return_dict, func):
    return_dict[procnum] = func(procnum)

def multiproc_loop():
    manager  = multiprocessing.Manager()
    
    fibo_dict = manager.dict()
    fact_dict = manager.dict()
    square_dict = manager.dict()
    cube_dict = manager.dict()

    fibo_proc = []
    fact_proc = []
    square_proc = []
    cube_proc = []
    
    for i in range(1, SIZE):
        tmp = multiprocessing.Process(target=worker, args=(i, fibo_dict, fibo_m))
        tmp.start()
        fibo_proc.append(tmp)

    for i in range(1, SIZE):
        tmp = multiprocessing.Process(target=worker, args=(i, fact_dict, fact_m))
        tmp.start()                                              
        fact_proc.append(tmp)

    for i in range(1, SIZE):
        tmp = multiprocessing.Process(target=worker, args=(i, square_dict, square_m))
        tmp.start()                                              
        square_proc.append(tmp)

    for i in range(1, SIZE):
        tmp = multiprocessing.Process(target=worker, args=(i, cube_dict, cube_m))
        tmp.start()                                              
        cube_proc.append(tmp)

    for (a, b, c, d) in zip(fibo_proc, fact_proc, square_proc, cube_proc):
        a.join()
        b.join()
        c.join()
        d.join()
    
    print(f"Result:\n\t{fibo_dict.values()},\n\t{fact_dict.values()}")
    print(f"Result:\n\t{square_dict.values()},\n\t{cube_dict.values()}")

start = time.perf_counter()

multiproc_loop()

duration = time.perf_counter() - start
print(f"\nMultiproc_duration = {duration}\n")
