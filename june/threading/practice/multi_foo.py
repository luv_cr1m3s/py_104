#import threading
import time
import concurrent.futures

array = list()

def foo_f(index):
    time.sleep(2)
    print(f"Slept for 2 secs in {index}")
    array.append(index ** 2)
    
if __name__ == "__main__":
    threads = list()

    with concurrent.futures.ThreadPoolExecutor(max_workers=3) as executor:
        executor.map(foo_f, range(3))
    
    print(array)

#    for i in range(3):
#        print(f"Create and start thread {i}")
#        x = threading.Thread(target=foo_f, args=(i,))
#        threads.append(x)
#        x.start()
#
#    for i, t in enumerate(threads):
#        print("Main before join()")
#        t.join()
#        print("Main after join()")

