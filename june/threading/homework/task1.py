#!/usr/bin/env python3

"""
Task 1

A shared counter

Make a class called Counter, and make it a subclass of the Thread class in the Threading module. Make the class have two global variables, one called counter set to 0, and another called rounds set to 100.000. Now implement the run() method, let it include a simple for-loop that iterates through rounds (e.i. 100.000 times) and for each time increments the value of the counter by 1. Create 2 instances of the thread and start them, then join them and check the result of the counter, it should be 200.000, right? Run it a couple of times and consider some different reasons why you get the answer that you get.
"""

from threading import Thread

class Counter(Thread):

    counter = 0
    rounds = 100000

    def run(self):
        for _ in range(self.rounds):
            self.counter += 1

a = Counter()
b = Counter()

print("b.counter (sometimes a.counter as well)  is less that 100k because we didn't wait fo rend of thread execution")
print("usage of .join() on both threads will fix it")

a.start()
b.start()

print(f"a : {a.counter}, b : {b.counter}")
