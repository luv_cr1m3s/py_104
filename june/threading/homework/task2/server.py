#!/usr/bin/env python3

"""
Task 2

Echo server with threading

Create a socket echo server which handles each connection in a separate Thread
"""

import threading
import socket

def thread_con(con):

    data = con.recv(1024)
    data = data.decode()
    
    if len(data) == 0:
        print("Nothing was sent")
    else:
        print("Sending data back->")
        data = data.upper().encode()
        con.send(data)

    con.close()

if __name__ == '__main__':

    PORT = 8888
    HOST = '127.0.0.1'

    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
        sock.bind((HOST, PORT))

        sock.listen(3)
        print(f"Server is listening on {HOST}:{PORT}")
    
        threads = list()

        for i in range(3):
            con, addr = sock.accept()

            print("Connected to : ", addr[0], ':', addr[1])
            tmp = threading.Thread(target = thread_con, args = (con, ))
            tmp.start()
            threads.append(tmp)
            print(f"Active threads = {threading.active_count()}")

        for i in range(3):
            threads[i].join()
