#!/usr/bin/env python3

import multiprocessing
import math
import time
from functools import wraps
from concurrent.futures import Executor, ProcessPoolExecutor, ThreadPoolExecutor
"""
NUMBERS = [
   2,  # prime
   1099726899285419,
   1570341764013157,  # prime
   1637027521802551,  # prime
   1880450821379411,  # prime
   1893530391196711,  # prime
   2447109360961063,  # prime
   3,  # prime
   2772290760589219,  # prime
   3033700317376073,  # prime
   4350190374376723,
   4350190491008389,  # prime
   4350190491008390,
   4350222956688319,
   2447120421950803,
   5,  # prime
]

We have the following input list of numbers, some of them are prime. You need to create a utility function that takes as input a number and returns a bool, whether it is prime or not.

 

Use ThreadPoolExecutor and ProcessPoolExecutor to create different concurrent implementations for filtering NUMBERS. 

Compare the results and performance of each of them.
"""
NUMBERS = [
   2,
   1099726899285419,
   1570341764013157,
   1637027521802551,
   1880450821379411,
   1893530391196711,
   2447109360961063,
   3,
   2772290760589219,
   3033700317376073,
   4350190374376723,
   4350190491008389,
   4350190491008390,
   4350222956688319,
   2447120421950803,
   5,
]

def timeit(func):
    @wraps(func)
    def timeit_wrapper(*args, **kwargs):
        start = time.perf_counter()
        result = func(*args, **kwargs)
        end = time.perf_counter()
        total = end - start
        print(f"Func {func.__name__} {args} Took {total} seconds, result: {result}")
        return result
    return timeit_wrapper

@timeit
def is_prime(n):
    for i in range(2, int(math.sqrt(n))+1):
        if n % i == 0:
            return False
    return True

@timeit
def threading_f(n):
    with ThreadPoolExecutor(max_workers = len(n)) as e:
        e.map(is_prime, n)

@timeit
def multiproc_f(n):
    with ProcessPoolExecutor(max_workers=len(n)) as e:
        e.map(is_prime, n)


if __name__ == '__main__':
   
    print("without anything:")
    start = time.perf_counter()
    for i in NUMBERS:
        is_prime(i)
    end = time.perf_counter()
    print(f"Took: {end - start}")

    print("\nusing threads: ")
    threading_f(NUMBERS)

    print("\nusing multiprocessing: ")
    multiproc_f(NUMBERS)
