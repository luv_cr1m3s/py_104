"""


Task 2

Requests using concurrent and multiprocessing libraries

Download all comments from a subreddit of your choice using URL: https://api.pushshift.io/reddit/comment/search/ . 

As a result, store all comments in chronological order in JSON and dump it to a file. For this task use concurrent and multiprocessing libraries for making requests to Reddit API.

"""
# pushshift is not available anymore
