#!/usr/bin/env python3
import socket
from threading import Thread

SERVER_HOST = 'localhost' 
SERVER_PORT = 8888

client_connections = set()

sock = socket.socket()
sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1) # try experiments !
sock.bind((SERVER_HOST, SERVER_PORT))
sock.listen(5)

list_of_clients = list()

print(f'Server ready on {SERVER_HOST}:{SERVER_PORT}')

def listen_for_message(connection):
   while True:
        try:
            message = connection.recv(1024)
            if message:
                broadcast(message, connection)
        except Exception as ex:
            print(f'Exception: {ex}')

def broadcast(message, connection):
    for client in list_of_clients:
        if client != connection:
            print(f"sending message to {client.getpeername()[1]}")
            send_msg = "->" + message.decode("utf-8")
            client.send(send_msg.encode("utf-8"))

while True:
    connection, client_address = sock.accept()
    list_of_clients.append(connection)

    print(f'connected from client address: {client_address}')
    client_connections.add(connection)
    task = Thread(target=listen_for_message, args=(connection,), daemon=True)
    task.start()
