import socket
import select
import sys

HOST = '127.0.0.1'  # The server's hostname or IP address
PORT = 8888 

client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
client_socket.connect((HOST, PORT))

while True:
    source = [sys.stdin, client_socket]
    
    read, write, error = select.select(source, [], [])

    for s  in read:
        if s == client_socket:
            message = client_socket.recv(1024)
            print(message.decode("utf-8"))
        else:
            message = input(": ")
            client_socket.send(message.encode("utf-8"))
            
            CURSOR_UP = '\033[F'
            ERASE_LINE = '\033[K'
            print(CURSOR_UP + ERASE_LINE + CURSOR_UP)

            print(f"<- {message}")


