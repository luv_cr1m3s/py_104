import os 
from bs4 import BeautifulSoup as soup
import logging
import requests
from telegram import Message, Update
from telegram.ext import ApplicationBuilder, ContextTypes, CommandHandler, MessageHandler, filters

TOKEN = os.getenv("TG_TOKEN") 
COUNTER = 1

logging.basicConfig(
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
    level=logging.INFO
)

def get_notes(note_id):
    req = requests.get(f'http://cr1m3s.pythonanywhere.com/note/{note_id}')
    text = "Nothing found"
    if req.status_code == 200:
        raw_text = soup(req.text)
        text = raw_text.find_all("div", class_ = "notetext")
        text = text[0].getText()

    return text

async def start(update: Update, context: ContextTypes.DEFAULT_TYPE):
    inp = update.message.text
    note_id = inp.split('/note ')[1] 
    note_text = get_notes(note_id)
    await context.bot.send_message(chat_id=update.effective_chat.id, text=note_text)

if __name__ == '__main__':
    application = ApplicationBuilder().token(TOKEN).build()
    
    start_handler = CommandHandler('note', start)
    application.add_handler(start_handler)
    
    application.run_polling()
