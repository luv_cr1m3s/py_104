import random

def generate(p1):
    # change this so that it generates 10000 random zeros and ones
    # where the probability of one is p1
    seq = []

    for i in range(0, 10000):
        rng = random.random()
        if rng < 1 - p1:
            seq.append(0)
        else:
            seq.append(1)

    return seq

def count(seq):
    result = 0
    flag = 0

    for i in seq:
        if i == 1:
            flag += 1
            if flag == 5:
                result += 1
                flag = 4
        else:
            flag = 0
    return result


def main(p1):
    seq = generate(p1)
    print(seq)
    return count(seq)

print(main(2/3))
