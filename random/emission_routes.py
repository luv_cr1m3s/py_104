portnames = ["PAN", "AMS", "CAS", "NYC", "HEL"]

def permutations(route, ports):
    # Write your recursive code here
    if len(ports)+1 == len(route):
        #route  = [portnames[i] for i in route]
        print_emisison(route)
    else:
        for i in range(1, 5):
            if i not in route:
                permutations(route + [i], ports)

def print_emisison(route):

    D = [
         [0,8943,8019,3652,10545],
         [8943,0,2619,6317,2078],
         [8019,2619,0,5836,4939],
         [3652,6317,5836,0,7825],
         [10545,2078,4939,7825,0]
        ]

    distance = D[route[0]][route[1]] + D[route[1]][route[2]] + D[route[2]][route[3]] + D[route[3]][route[4]]
    emissions = distance * 0.02
    print(' '.join([portnames[i] for i in route]) + " %.1f kg" % emissions)

def main():
    permutations([0], list(range(1, len(portnames))))

main()
