import numpy as np

p1 = [1/6, 1/6, 1/6, 1/6, 1/6, 1/6]   # normal
p2 = [0.1, 0.1, 0.1, 0.1, 0.1, 0.5]   # loaded

def roll(loaded):
    if loaded:
        print("rolling a loaded die")
        p = p2
    else:
        print("rolling a normal die")
        p = p1

    # roll the dice 10 times
    # add 1 to get dice rolls from 1 to 6 instead of 0 to 5
    sequence = np.random.choice(6, size=10, p=p) + 1 
    for roll in sequence:
        print("rolled %d" % roll)
        
    return sequence

def bayes(sequence):
    loaded_odds = 1.0           # start with odds 1:1
    normal_odds = 1.0
    for roll in sequence:
        if roll == 6:
            loaded_odds *= p2[roll - 1]
            normal_odds *= 1/6
        else:
            loaded_odds *= 0.1          # edit here to update the odds
            normal_odds *= 1/6
    if loaded_odds > normal_odds:
        return True
    else:
        return False

sequence = roll(True)
if bayes(sequence):
    print("I think loaded")
else:
    print("I think normal")

